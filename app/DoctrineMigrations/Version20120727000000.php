<?php

/*
 * v0.1
 */

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20120727000000 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql");
        if ($this->connection->getDatabasePlatform()->getName() == "postgresql") {
            $this->addSql("CREATE SEQUENCE setup_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE SEQUENCE page_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE SEQUENCE journal_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE SEQUENCE reward_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE SEQUENCE character_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE SEQUENCE account_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE SEQUENCE rpg_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE SEQUENCE rpg_user_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE SEQUENCE quest_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE SEQUENCE rpg_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
            $this->addSql("CREATE TABLE setup (id INT NOT NULL, copyright_holder VARCHAR(255) NOT NULL, copyright_holder_email VARCHAR(255) NOT NULL, domain VARCHAR(255) NOT NULL, game_contact_email VARCHAR(255) NOT NULL, game_name VARCHAR(255) NOT NULL, statistics_enabled INT NOT NULL, statistics_key VARCHAR(255) DEFAULT NULL, test_mode INT NOT NULL, theme VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE TABLE page (id INT NOT NULL, content TEXT NOT NULL, locale VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, is_visible INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE INDEX page_main_01_index ON page (id, locale, name, type, is_visible, created_at, updated_at)");
            $this->addSql("CREATE TABLE journal (id INT NOT NULL, character_id INT NOT NULL, quest_id INT NOT NULL, user_id INT NOT NULL, is_completed INT NOT NULL, is_hidden INT NOT NULL, is_ordered_by_someone_else INT NOT NULL, is_visible INT NOT NULL, completed_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE INDEX IDX_C1A7E74D1136BE75 ON journal (character_id)");
            $this->addSql("CREATE INDEX IDX_C1A7E74D209E9EF4 ON journal (quest_id)");
            $this->addSql("CREATE INDEX IDX_C1A7E74DA76ED395 ON journal (user_id)");
            $this->addSql("CREATE INDEX journal_main_01_index ON journal (id, character_id, quest_id, user_id, is_completed, is_hidden, is_ordered_by_someone_else, is_visible, completed_at, created_at, updated_at)");
            $this->addSql("CREATE TABLE reward (id INT NOT NULL, character_id INT NOT NULL, user_id INT NOT NULL, content TEXT NOT NULL, level INT NOT NULL, photo VARCHAR(255) DEFAULT NULL, title VARCHAR(255) NOT NULL, is_awarded INT NOT NULL, is_blocked INT NOT NULL, is_visible INT NOT NULL, awarded_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE INDEX IDX_4ED172531136BE75 ON reward (character_id)");
            $this->addSql("CREATE INDEX IDX_4ED17253A76ED395 ON reward (user_id)");
            $this->addSql("CREATE INDEX reward_main_01_index ON reward (id, character_id, user_id, level, is_awarded, is_blocked, is_visible, awarded_at, created_at, updated_at)");
            $this->addSql("CREATE TABLE character (id INT NOT NULL, user_id INT UNIQUE NOT NULL, attribute_agility DOUBLE PRECISION NOT NULL, attribute_charisma DOUBLE PRECISION NOT NULL, attribute_endurance DOUBLE PRECISION NOT NULL, attribute_intelligence DOUBLE PRECISION NOT NULL, attribute_luck DOUBLE PRECISION NOT NULL, attribute_perception DOUBLE PRECISION NOT NULL, attribute_strength DOUBLE PRECISION NOT NULL, attribute_willpower DOUBLE PRECISION NOT NULL, attribute_wisdom DOUBLE PRECISION NOT NULL, avatar VARCHAR(255) DEFAULT NULL, birth_date DATE DEFAULT NULL, experience INT NOT NULL, gender VARCHAR(255) DEFAULT NULL, level INT NOT NULL, name VARCHAR(255) DEFAULT NULL, skill_art DOUBLE PRECISION NOT NULL, skill_culture DOUBLE PRECISION NOT NULL, skill_entertainment DOUBLE PRECISION NOT NULL, skill_helpfulness DOUBLE PRECISION NOT NULL, skill_home DOUBLE PRECISION NOT NULL, skill_learning DOUBLE PRECISION NOT NULL, skill_recreation DOUBLE PRECISION NOT NULL, skill_sport DOUBLE PRECISION NOT NULL, skill_work DOUBLE PRECISION NOT NULL, is_visible INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE UNIQUE INDEX UNIQ_937AB034A76ED395 ON character (user_id)");
            $this->addSql("CREATE INDEX character_main_01_index ON character (id, user_id, birth_date, experience, gender, level, name, is_visible, created_at, updated_at)");
            $this->addSql("CREATE INDEX character_main_02_index ON character (attribute_agility, attribute_charisma, attribute_endurance, attribute_intelligence, attribute_luck, attribute_perception, attribute_strength, attribute_willpower, attribute_wisdom)");
            $this->addSql("CREATE INDEX character_main_03_index ON character (skill_art, skill_culture, skill_entertainment, skill_helpfulness, skill_home, skill_learning, skill_recreation, skill_sport, skill_work)");
            $this->addSql("CREATE TABLE account (id INT NOT NULL, user_id INT UNIQUE NOT NULL, type VARCHAR(255) NOT NULL, expires_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE UNIQUE INDEX UNIQ_7D3656A4A76ED395 ON account (user_id)");
            $this->addSql("CREATE INDEX account_main_01_index ON account (id, user_id, type, expires_at, created_at, updated_at)");
            $this->addSql("CREATE TABLE rpg_group (id INT NOT NULL, name VARCHAR(255) UNIQUE NOT NULL, roles TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE UNIQUE INDEX UNIQ_8C2715FC5E237E06 ON rpg_group (name)");
            $this->addSql("CREATE INDEX group_main_01_index ON rpg_group (id, name, created_at, updated_at)");
            $this->addSql("COMMENT ON COLUMN rpg_group.roles IS '(DC2Type:array)'");
            $this->addSql("CREATE TABLE rpg_user_group (id INT NOT NULL, group_id INT NOT NULL, user_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE INDEX IDX_BB4A0925FE54D947 ON rpg_user_group (group_id)");
            $this->addSql("CREATE INDEX IDX_BB4A0925A76ED395 ON rpg_user_group (user_id)");
            $this->addSql("CREATE INDEX user_group_main_01_index ON rpg_user_group (id, group_id, user_id, created_at, updated_at)");
            $this->addSql("CREATE TABLE session (session_id VARCHAR(255) NOT NULL, session_value TEXT NOT NULL, session_time INT NOT NULL, PRIMARY KEY(session_id))");
            $this->addSql("CREATE TABLE quest (id INT NOT NULL, character_id INT NOT NULL, user_id INT NOT NULL, attribute_reward VARCHAR(255) NOT NULL, content TEXT NOT NULL, skill_reward VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, is_blocked INT NOT NULL, is_group_quest INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE INDEX IDX_4317F8171136BE75 ON quest (character_id)");
            $this->addSql("CREATE INDEX IDX_4317F817A76ED395 ON quest (user_id)");
            $this->addSql("CREATE INDEX quest_main_01_index ON quest (id, character_id, user_id, attribute_reward, skill_reward, is_blocked, is_group_quest, created_at, updated_at)");
            $this->addSql("CREATE TABLE rpg_user (id INT NOT NULL, username VARCHAR(255) NOT NULL, username_canonical VARCHAR(255) UNIQUE NOT NULL, email VARCHAR(255) NOT NULL, email_canonical VARCHAR(255) UNIQUE NOT NULL, enabled BOOLEAN NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, locked BOOLEAN NOT NULL, expired BOOLEAN NOT NULL, expires_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, roles TEXT NOT NULL, credentials_expired BOOLEAN NOT NULL, credentials_expire_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("CREATE UNIQUE INDEX UNIQ_645AF70292FC23A8 ON rpg_user (username_canonical)");
            $this->addSql("CREATE UNIQUE INDEX UNIQ_645AF702A0D96FBF ON rpg_user (email_canonical)");
            $this->addSql("CREATE INDEX iser_main_01_index ON rpg_user (id, username, username_canonical, email, email_canonical, enabled, last_login, locked, expired, expires_at, password_requested_at, created_at, updated_at)");
            $this->addSql("COMMENT ON COLUMN rpg_user.roles IS '(DC2Type:array)'");
            $this->addSql("ALTER TABLE journal ADD CONSTRAINT FK_C1A7E74D1136BE75 FOREIGN KEY (character_id) REFERENCES character (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE journal ADD CONSTRAINT FK_C1A7E74D209E9EF4 FOREIGN KEY (quest_id) REFERENCES quest (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE journal ADD CONSTRAINT FK_C1A7E74DA76ED395 FOREIGN KEY (user_id) REFERENCES rpg_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE reward ADD CONSTRAINT FK_4ED172531136BE75 FOREIGN KEY (character_id) REFERENCES character (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE reward ADD CONSTRAINT FK_4ED17253A76ED395 FOREIGN KEY (user_id) REFERENCES rpg_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE character ADD CONSTRAINT FK_937AB034A76ED395 FOREIGN KEY (user_id) REFERENCES rpg_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE account ADD CONSTRAINT FK_7D3656A4A76ED395 FOREIGN KEY (user_id) REFERENCES rpg_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE rpg_user_group ADD CONSTRAINT FK_BB4A0925FE54D947 FOREIGN KEY (group_id) REFERENCES rpg_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE rpg_user_group ADD CONSTRAINT FK_BB4A0925A76ED395 FOREIGN KEY (user_id) REFERENCES rpg_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE quest ADD CONSTRAINT FK_4317F8171136BE75 FOREIGN KEY (character_id) REFERENCES character (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("ALTER TABLE quest ADD CONSTRAINT FK_4317F817A76ED395 FOREIGN KEY (user_id) REFERENCES rpg_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        }
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql");
        if ($this->connection->getDatabasePlatform()->getName() == "postgresql") {
            $this->addSql("ALTER TABLE journal DROP CONSTRAINT FK_C1A7E74D1136BE75");
            $this->addSql("ALTER TABLE reward DROP CONSTRAINT FK_4ED172531136BE75");
            $this->addSql("ALTER TABLE quest DROP CONSTRAINT FK_4317F8171136BE75");
            $this->addSql("ALTER TABLE rpg_user_group DROP CONSTRAINT FK_BB4A0925FE54D947");
            $this->addSql("ALTER TABLE journal DROP CONSTRAINT FK_C1A7E74D209E9EF4");
            $this->addSql("ALTER TABLE journal DROP CONSTRAINT FK_C1A7E74DA76ED395");
            $this->addSql("ALTER TABLE reward DROP CONSTRAINT FK_4ED17253A76ED395");
            $this->addSql("ALTER TABLE character DROP CONSTRAINT FK_937AB034A76ED395");
            $this->addSql("ALTER TABLE account DROP CONSTRAINT FK_7D3656A4A76ED395");
            $this->addSql("ALTER TABLE rpg_user_group DROP CONSTRAINT FK_BB4A0925A76ED395");
            $this->addSql("ALTER TABLE quest DROP CONSTRAINT FK_4317F817A76ED395");
            $this->addSql("DROP SEQUENCE setup_id_seq");
            $this->addSql("DROP SEQUENCE page_id_seq");
            $this->addSql("DROP SEQUENCE journal_id_seq");
            $this->addSql("DROP SEQUENCE reward_id_seq");
            $this->addSql("DROP SEQUENCE character_id_seq");
            $this->addSql("DROP SEQUENCE account_id_seq");
            $this->addSql("DROP SEQUENCE rpg_group_id_seq");
            $this->addSql("DROP SEQUENCE rpg_user_group_id_seq");
            $this->addSql("DROP SEQUENCE quest_id_seq");
            $this->addSql("DROP SEQUENCE rpg_user_id_seq");
            $this->addSql("DROP TABLE setup");
            $this->addSql("DROP TABLE page");
            $this->addSql("DROP TABLE journal");
            $this->addSql("DROP TABLE reward");
            $this->addSql("DROP TABLE character");
            $this->addSql("DROP TABLE account");
            $this->addSql("DROP TABLE rpg_group");
            $this->addSql("DROP TABLE rpg_user_group");
            $this->addSql("DROP TABLE session");
            $this->addSql("DROP TABLE quest");
            $this->addSql("DROP TABLE rpg_user");
        }
    }
}
