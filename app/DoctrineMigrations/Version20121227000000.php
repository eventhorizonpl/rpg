<?php

/*
 * v0.4
 */

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20121227000000 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql");
        if ($this->connection->getDatabasePlatform()->getName() == "postgresql") {
            $this->addSql("ALTER TABLE journal ADD journal_id INT DEFAULT NULL");
            $this->addSql("ALTER TABLE journal ADD completed_quest_counter INT NOT NULL DEFAULT 0");
            $this->addSql("ALTER TABLE journal ADD quest_counter INT NOT NULL DEFAULT 0");
            $this->addSql("ALTER TABLE journal ADD CONSTRAINT FK_C1A7E74D478E8802 FOREIGN KEY (journal_id) REFERENCES journal (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
            $this->addSql("CREATE INDEX IDX_C1A7E74D478E8802 ON journal (journal_id)");
            $this->addSql("CREATE INDEX journal_02_index ON journal (journal_id, completed_quest_counter, quest_counter)");
            $this->addSql("ALTER TABLE journal ALTER COLUMN completed_quest_counter SET DEFAULT NULL");
            $this->addSql("ALTER TABLE journal ALTER COLUMN quest_counter SET DEFAULT NULL");
            $this->addSql("ALTER TABLE quest ADD type VARCHAR(255) NOT NULL DEFAULT 'standard'");
            $this->addSql("CREATE INDEX quest_01_index ON quest (type)");
            $this->addSql("ALTER TABLE quest ALTER COLUMN type SET DEFAULT NULL");
        }
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql");
        if ($this->connection->getDatabasePlatform()->getName() == "postgresql") {
            $this->addSql("DROP INDEX quest_01_index");
            $this->addSql("ALTER TABLE quest DROP type");
            $this->addSql("ALTER TABLE journal DROP CONSTRAINT FK_C1A7E74D478E8802");
            $this->addSql("DROP INDEX IDX_C1A7E74D478E8802");
            $this->addSql("DROP INDEX journal_02_index");
            $this->addSql("ALTER TABLE journal DROP journal_id");
            $this->addSql("ALTER TABLE journal DROP completed_quest_counter");
            $this->addSql("ALTER TABLE journal DROP quest_counter");
        }
    }
}
