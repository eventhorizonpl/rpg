<?php

/*
 * v0.2
 */

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20121005000000 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql");
        if ($this->connection->getDatabasePlatform()->getName() == "postgresql") {
            $this->addSql("ALTER TABLE rpg_user ADD facebook_id VARCHAR(255) DEFAULT NULL");
        }
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql");
        if ($this->connection->getDatabasePlatform()->getName() == "postgresql") {
            $this->addSql("ALTER TABLE rpg_user DROP facebook_id");
        }
    }
}
