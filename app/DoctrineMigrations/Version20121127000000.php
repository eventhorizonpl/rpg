<?php

/*
 * v0.3
 */

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20121127000000 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql");
        if ($this->connection->getDatabasePlatform()->getName() == "postgresql") {
            $this->addSql("CREATE TABLE details (id SERIAL NOT NULL, url VARCHAR(255) DEFAULT NULL, c_url VARCHAR(255) DEFAULT NULL, \"server name\" VARCHAR(64) DEFAULT NULL, type INT DEFAULT NULL, perfdata BYTEA NOT NULL, cookie BYTEA NOT NULL, post BYTEA NOT NULL, get BYTEA NOT NULL, pmu INT DEFAULT NULL, wt INT DEFAULT NULL, cpu INT DEFAULT NULL, server_id VARCHAR(3) NOT NULL, aggregateCalls_include VARCHAR(255) DEFAULT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))");
            $this->addSql("ALTER TABLE account ADD default_journal_is_visible INT NOT NULL DEFAULT 0");
            $this->addSql("ALTER TABLE account ADD default_journal_publish_on_facebook INT NOT NULL DEFAULT 0");
            $this->addSql("ALTER TABLE account ADD default_reward_is_visible INT NOT NULL DEFAULT 0");
            $this->addSql("ALTER TABLE reward ADD is_received INT NOT NULL DEFAULT 0");
            $this->addSql("ALTER TABLE journal ADD publish_on_facebook INT NOT NULL DEFAULT 0");
            $this->addSql("ALTER TABLE account ALTER COLUMN default_journal_is_visible SET DEFAULT NULL");
            $this->addSql("ALTER TABLE account ALTER COLUMN default_journal_publish_on_facebook SET DEFAULT NULL");
            $this->addSql("ALTER TABLE account ALTER COLUMN default_reward_is_visible SET DEFAULT NULL");
            $this->addSql("ALTER TABLE reward ALTER COLUMN is_received SET DEFAULT NULL");
            $this->addSql("ALTER TABLE journal ALTER COLUMN publish_on_facebook SET DEFAULT NULL");
            $this->addSql("CREATE INDEX account_01_index ON account (default_journal_is_visible, default_journal_publish_on_facebook, default_reward_is_visible)");
            $this->addSql("CREATE INDEX reward_01_index ON reward (is_received)");
            $this->addSql("CREATE INDEX journal_01_index ON journal (publish_on_facebook)");
        }
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql");
        if ($this->connection->getDatabasePlatform()->getName() == "postgresql") {
            $this->addSql("DROP TABLE details");
            $this->addSql("DROP INDEX journal_01_index");
            $this->addSql("DROP INDEX reward_01_index");
            $this->addSql("DROP INDEX account_01_index");
            $this->addSql("ALTER TABLE journal DROP publish_on_facebook");
            $this->addSql("ALTER TABLE reward DROP is_received");
            $this->addSql("ALTER TABLE account DROP default_journal_is_visible");
            $this->addSql("ALTER TABLE account DROP default_journal_publish_on_facebook");
            $this->addSql("ALTER TABLE account DROP default_reward_is_visible");
        }
    }
}
