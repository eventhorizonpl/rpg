CSS = ./web/bundles/eventhorizonrpg/css/
JS = ./web/bundles/eventhorizonrpg/js/
JS_01 = ${JS}/jquery.js
JS_02 = ${JS}/jquery.cookie.js
JS_10 = ${JS}/bootstrap-transition.js
JS_11 = ${JS}/bootstrap-alert.js
//JS_12 = ${JS}/bootstrap-button.js
JS_13 = ${JS}/bootstrap-carousel.js
JS_14 = ${JS}/bootstrap-collapse.js
JS_15 = ${JS}/bootstrap-dropdown.js
JS_16 = ${JS}/bootstrap-modal.js
JS_17 = ${JS}/bootstrap-tooltip.js
JS_18 = ${JS}/bootstrap-popover.js
//JS_19 = ${JS}/bootstrap-scrollspy.js
JS_20 = ${JS}/bootstrap-tab.js
//JS_21 = ${JS}/bootstrap-typeahead.js
JS_30 = ${JS}/confirm-bootstrap.js
JS_31 = ${JS}/bootstrap-datepicker.js
JS_40 = ${JS}/layout1.js
LESS = ./web/bundles/eventhorizonrpg/less/

all: css js

assets_install:
	app/console assets:install web

css: assets_install
	lessc --compress ${LESS}/rpg.less > ${CSS}/main.css

js: assets_install
	cat ${JS_01} ${JS_02} ${JS_10} ${JS_11} ${JS_12} ${JS_13} ${JS_14} ${JS_15} ${JS_16} ${JS_17} ${JS_18} ${JS_19} ${JS_20} ${JS_21} ${JS_30} ${JS_31} ${JS_40} > ${JS}/main.js
	#uglifyjs -nc ${JS}/main.js > ${JS}/main.min.js
	java -jar ~/bin/yuicompressor-2.4.7.jar --type js -o '${JS}/main.min.js' ${JS}/main.js
	mv ${JS}/main.min.js ${JS}/main.js
