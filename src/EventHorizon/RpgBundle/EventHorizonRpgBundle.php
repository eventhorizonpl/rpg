<?php

namespace EventHorizon\RpgBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EventHorizonRpgBundle extends Bundle
{
    public static function addNotice($message, $log, $request)
    {
        $request->trustProxyData();
        $ip = $request->getClientIp();
        $message = $ip." ".$message;
        $log->addNotice($message);
    }

    public static function getAttributeRewardPoints()
    {
        return 0.1;
    }

    public static function getExperienceRewardPoints()
    {
        return 1;
    }

    public static function getPromotionLevel()
    {
        return 10;
    }

    public static function getSkillRewardPoints()
    {
        return 0.5;
    }
}
