<?php

namespace EventHorizon\RpgBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BaseTest extends WebTestCase
{
    public function getHostUrl($hostId, $path = '')
    {
        return self::$kernel->getContainer()->getParameter('rpg.hostname.'.$hostId).$path;
    }

    public function testSomething()
    {
        $this->markTestIncomplete('This test has not been implemented yet.');
    }
}
