<?php

namespace EventHorizon\RpgBundle\Tests\Controller;

use EventHorizon\RpgBundle\Tests\Controller\BaseTest;

class ShowControllerTest extends BaseTest
{
    public function testStatisticsShow()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $this->getHostUrl('test1', '/show/statistics'));

        $this->assertGreaterThan(0, $crawler->filter('html:contains("getACTIVE!")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Statystyki")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Ilość ukończonych zadań w grze")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Ilość ukrytych zadań w grze")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Ilość widocznych dla wszystkich zadań w grze")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Ilość oczekujących na ukończenie zadań w grze")')->count());
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Ilość wszystkich zadań w grze")')->count());
    }
}
