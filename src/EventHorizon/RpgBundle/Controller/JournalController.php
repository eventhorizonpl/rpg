<?php

namespace EventHorizon\RpgBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EventHorizon\RpgBundle\EventHorizonRpgBundle;
use EventHorizon\RpgBundle\Entity\Journal;

class JournalController extends Controller
{
    public function common()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->user = $this->container->get('security.context')->getToken()->getUser();
        $this->user_id = $this->user->getId();
        $this->request = $this->getRequest();
        $this->request->setLocale($this->get('session')->get('_locale'));
        $this->request->getSession()->set('referrer', $this->request->getRequestUri());
    }

    public function completeAction($id)
    {
        $this->common();
        $is_completed = 0;
        $this->journal = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalByIdAndUserIdAndIsCompletedV1($id, $this->user_id, $is_completed);

        if (!($this->journal)) {
            $message = "EventHorizonRpgBundle:Journal:complete id=".$id." user_id=".$this->user_id." is_completed=".$is_completed;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Journal entity.');
        }

        $is_visible = $this->journal->getIsVisible();
        $publish_on_facebook = $this->journal->getPublishOnFacebook();

        $character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdV2($this->user_id);

        $level = ($character->getLevel() + 1);
        $reward = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardsByUserIdAndLevelV1($this->user_id, $level);

        if (!($reward)) {
            $this->get('session')->setFlash('info', $this->get('translator')->trans('Musisz wyznaczyć nagrodę za osiągnięcie kolejnego poziomu'));

            return $this->redirect($this->generateUrl('reward_new'));
        }

        $reward_awarded = $this->em->getRepository('EventHorizonRpgBundle:Journal')->completeQuest($this->journal, $this->user);

        $this->get('session')->setFlash('info', $this->get('translator')->trans('Zadanie zostało zakończone'));

        $this->publishOnFacebook($is_visible, $publish_on_facebook);

        if ($reward_awarded) {
            return $this->redirect($this->generateUrl('reward_show_awarded', array('id' => $reward_awarded)));
        } else {
            return $this->redirect($this->generateUrl('journal', array('mode' => 'todo')));
        }
    }

    public function publishOnFacebook($is_visible, $publish_on_facebook)
    {
        if (($is_visible) and ($publish_on_facebook) and ($this->user->getFacebookId()) and ($this->user->getCharacter()->getIsVisible())) {
            $facebookSdk = $this->get("armetiz.facebook.rpg");
            $accessToken = $facebookSdk->getAccessToken();
            $facebookSdk->setAccessToken($accessToken);
            $link = $this->generateUrl('show_journal', array('journal_id' => $this->journal->getId(), 'user_id' => $this->user_id), true);
            $messge = $this->user->getCharacter()->getName()." ".$this->get('translator')->trans('z powodzeniem kończy wyzwanie');

            try {
                $facebookSdk->api('/me/feed', 'POST', array(
                    'link' => $link,
                    'message' => $messge
                ));
            } catch (\Exception $e) {
                $this->get('session')->setFlash('error', $this->get('translator')->trans('Brak uprawnień do publikacji na Facebooku'));
            }
        }
    }

    public function hideAction($id)
    {
        $this->common();
        $is_completed = 0;
        $journal = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalByIdAndUserIdAndIsCompletedV1($id, $this->user_id, $is_completed);

        if (!($journal)) {
            $message = "EventHorizonRpgBundle:Journal:hide id=".$id." user_id=".$this->user_id." is_completed=".$is_completed;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Journal entity.');
        }

        $this->em->getRepository('EventHorizonRpgBundle:Journal')->hideQuest($journal, $this->user);

        $this->get('session')->setFlash('success', $this->get('translator')->trans('Zadanie zostało ukryte'));

        return $this->redirect($this->generateUrl('journal', array('mode' => 'todo')));
    }

    public function indexAction($mode, $type)
    {
        $this->common();
        if ($mode == 'todo') {
            $is_completed = 0;
        } else {
            $is_completed = 1;
        }
        if ($type == 'all') {
            $journals = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalsByUserIdAndIsCompletedV1($this->user_id, $is_completed);
        } else {
            $journals = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalsByUserIdAndTypeAndIsCompletedV1($this->user_id, $type, $is_completed);
        }

        return $this->render('EventHorizonRpgBundle:Journal:JournalsList.html.twig', array(
            'is_completed' => $is_completed,
            'journals'     => $journals,
            'mode'         => $mode,
            'setup'        => $this->setup,
            'type'         => $type,
        ));
    }

    public function showAction($id)
    {
        $this->common();
        $journal = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalByIdAndUserIdV1($id, $this->user_id);

        if (!($journal)) {
            $message = "EventHorizonRpgBundle:Journal:show id=".$id." user_id=".$this->user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Journal entity.');
        }

        return $this->render('EventHorizonRpgBundle:Journal:JournalShow.html.twig', array(
            'journal' => $journal,
            'setup'   => $this->setup,
        ));
    }
}
