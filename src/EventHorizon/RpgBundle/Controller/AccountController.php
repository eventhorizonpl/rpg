<?php

namespace EventHorizon\RpgBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EventHorizon\RpgBundle\EventHorizonRpgBundle;
use EventHorizon\RpgBundle\Form\AccountType;

/**
 * @Route("/account")
 */
class AccountController extends Controller
{
    public function common()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->user = $this->container->get('security.context')->getToken()->getUser();
        $this->user_id = $this->user->getId();
        $this->request = $this->getRequest();
        $this->request->setLocale($this->get('session')->get('_locale'));
        $this->request->getSession()->set('referrer', $this->request->getRequestUri());
    }

    public function EditCommon()
    {
        $this->common();
        $this->account = $this->em->getRepository('EventHorizonRpgBundle:Account')->getAccountByUserIdV1($this->user_id);
        if (!($this->account)) {
            $message = "EventHorizonRpgBundle:Account:EditCommon user_id=".$this->user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Account entity.');
        }
        $this->form = $this->createForm(new AccountType(array('user' => $this->user)), $this->account);
    }

    /**
     * @Method("get")
     * @Route("/edit", name="account_edit")
     * @Secure(roles="ROLE_USER")
     * @Template("EventHorizonRpgBundle:Account:Edit.html.twig")
     */
    public function EditAction()
    {
        $this->EditCommon();

        return array(
            'account' => $this->account,
            'form'    => $this->form->createView(),
            'user'    => $this->user,
            'setup'   => $this->setup,
        );
    }

    /**
     * @Method("post")
     * @Route("/edit", name="account_update")
     * @Secure(roles="ROLE_USER")
     * @Template("EventHorizonRpgBundle:Account:Edit.html.twig")
     */
    public function UpdateAction()
    {
        $this->EditCommon();

        $this->form->bind($this->request);

        if ($this->form->isValid()) {
            $this->em->getRepository('EventHorizonRpgBundle:Account')->saveEditAccount($this->account, $this->user);

            return $this->redirect($this->generateUrl('character_show'));
        }

        return $this->render('EventHorizonRpgBundle:Character:Edit.html.twig', array(
            'account' => $this->account,
            'form'    => $this->form->createView(),
            'user'    => $this->user,
            'setup'   => $this->setup,
        ));
    }
}
