<?php

namespace EventHorizon\RpgBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EventHorizon\RpgBundle\EventHorizonRpgBundle;
use EventHorizon\RpgBundle\Entity\Reward;
use EventHorizon\RpgBundle\Form\RewardType;

class RewardController extends Controller
{
    public function common()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->user = $this->container->get('security.context')->getToken()->getUser();
        $this->user_id = $this->user->getId();
        $this->request = $this->getRequest();
        $this->request->setLocale($this->get('session')->get('_locale'));
        $this->request->getSession()->set('referrer', $this->request->getRequestUri());
    }

    public function indexAction()
    {
        $this->common();
        $rewards = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardsByUserIdV1($this->user_id);
        $character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdV2($this->user_id);
        $level = ($character->getLevel() + 1);
        $reward = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardsByUserIdAndLevelV1($this->user_id, $level);

        if ($reward) {
            $new_reward = 0;
        } else {
            $new_reward = 1;
        }

        return $this->render('EventHorizonRpgBundle:Reward:RewardsList.html.twig', array(
            'level'      => $level,
            'new_reward' => $new_reward,
            'rewards'    => $rewards,
            'setup'      => $this->setup,
        ));
    }

    public function NewCommon()
    {
        $this->common();
        $this->character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdV2($this->user_id);
        $this->level = ($this->character->getLevel() + 1);
        $this->rewardExists = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardsByUserIdAndLevelV1($this->user_id, $this->level);

        $this->reward = new Reward();
        $this->form = $this->createForm(new RewardType(array('user' => $this->user)), $this->reward);
    }

    public function newAction()
    {
        $this->NewCommon();

        if ($this->rewardExists) {
            $this->get('session')->setFlash('info', $this->get('translator')->trans('Masz już wybraną nagrodę za osiągnięcie poziomu'));

            return $this->redirect($this->generateUrl('reward'));
        }

        return $this->render('EventHorizonRpgBundle:Reward:New.html.twig', array(
            'form'   => $this->form->createView(),
            'reward' => $this->reward,
            'setup'  => $this->setup,
        ));
    }

    public function createAction()
    {
        $this->NewCommon();

        if ($this->rewardExists) {
            $this->get('session')->setFlash('info', $this->get('translator')->trans('Masz już wybraną nagrodę za osiągnięcie poziomu'));

            return $this->redirect($this->generateUrl('reward'));
        }

        $this->form->bind($this->request);

        if ($this->form->isValid()) {
            $this->em->getRepository('EventHorizonRpgBundle:Reward')->saveNewReward($this->reward, $this->user);

            return $this->redirect($this->generateUrl('reward'));
        }

        return $this->render('EventHorizonRpgBundle:Reward:New.html.twig', array(
            'form'   => $this->form->createView(),
            'reward' => $this->reward,
            'setup'  => $this->setup,
        ));
    }

    public function EditCommon($id)
    {
        $this->common();
        $is_awarded = 0;

        $this->reward = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardByIdAndUserIdAndIsAwardedV1($id, $this->user_id, $is_awarded);

        if (!($this->reward)) {
            $this->message = "EventHorizonRpgBundle:Reward:EditCommon id=".$id." user_id=".$this->user_id." is_awarded=".$is_awarded;
            EventHorizonRpgBundle::addNotice($this->message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Reward entity.');
        }

        $this->form = $this->createForm(new RewardType(), $this->reward);
    }

    public function editAction($id)
    {
        $this->EditCommon($id);

        return $this->render('EventHorizonRpgBundle:Reward:Edit.html.twig', array(
            'form'   => $this->form->createView(),
            'reward' => $this->reward,
            'setup'  => $this->setup,
        ));
    }

    public function updateAction($id)
    {
        $this->EditCommon($id);

        $this->form->bind($this->request);

        if ($this->form->isValid()) {
            $this->em->getRepository('EventHorizonRpgBundle:Reward')->saveEditReward($this->reward, $this->user);

            return $this->redirect($this->generateUrl('reward'));
        }

        return $this->render('EventHorizonRpgBundle:Reward:Edit.html.twig', array(
            'form'   => $this->form->createView(),
            'reward' => $this->reward,
            'setup'  => $this->setup,
        ));
    }

    public function receiveAction($id)
    {
        $this->common();
        $is_awarded = 1;
        $is_received = 0;

        $reward = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardByIdAndUserIdAndIsAwardedAndIsReceivedV1($id, $this->user_id, $is_awarded, $is_received);

        if (!($reward)) {
            $message = "EventHorizonRpgBundle:Reward:receive id=".$id." user_id=".$this->user_id." is_awarded=".$is_awarded." is_received=".$is_received;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Reward entity.');
        }

        $this->em->getRepository('EventHorizonRpgBundle:Reward')->receiveReward($reward, $this->user);

        $this->get('session')->setFlash('info', $this->get('translator')->trans('Nagroda została oznaczona jako odebrana'));

        return $this->redirect($this->generateUrl('reward'));
    }

    public function showAction($id)
    {
        $this->common();
        $reward = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardByIdAndUserIdV1($id, $this->user_id);

        if (!($reward)) {
            $message = "EventHorizonRpgBundle:Reward:show id=".$id." user_id=".$this->user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Reward entity.');
        }

        return $this->render('EventHorizonRpgBundle:Reward:RewardShow.html.twig', array(
            'is_awarded' => 0,
            'reward'     => $reward,
            'setup'      => $this->setup,
        ));
    }

    public function showAwardedAction($id)
    {
        $this->common();
        $is_awarded = 1;

        $reward = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardByIdAndUserIdAndIsAwardedV1($id, $this->user_id, $is_awarded);

        if (!($reward)) {
            $message = "EventHorizonRpgBundle:Reward:showAwarded id=".$id." user_id=".$this->user_id." is_awarded=".$is_awarded;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Reward entity.');
        }

        return $this->render('EventHorizonRpgBundle:Reward:RewardShow.html.twig', array(
            'is_awarded' => $is_awarded,
            'reward'     => $reward,
            'setup'      => $this->setup,
        ));
    }
}
