<?php

namespace EventHorizon\RpgBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EventHorizon\RpgBundle\EventHorizonRpgBundle;
use EventHorizon\RpgBundle\Entity\Character;
use EventHorizon\RpgBundle\Form\CharacterType;

class CharacterController extends Controller
{
    public function common()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->user = $this->container->get('security.context')->getToken()->getUser();
        $this->user_id = $this->user->getId();
        $this->request = $this->getRequest();
        $this->request->setLocale($this->get('session')->get('_locale'));
        $this->request->getSession()->set('referrer', $this->request->getRequestUri());
    }

    public function showAction()
    {
        $this->common();

        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('_homepage'));
        }

        $character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdV1($this->user_id);
        if (!($character)) {
            $message = "EventHorizonRpgBundle:Character:show user_id=".$this->user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Character entity.');
        }

        $experience = $character->getExperience();
        $level = $character->getLevel();

        $promotion_level = EventHorizonRpgBundle::getPromotionLevel();
        $progress = ((($experience / $promotion_level) - ($level - 1)) * 100);
        $required_experience = ($level * $promotion_level);

        return $this->render('EventHorizonRpgBundle:Character:CharacterShow.html.twig', array(
            'character'           => $character,
            'setup'               => $this->setup,
            'progress'            => $progress,
            'required_experience' => $required_experience,
        ));
    }

    public function EditCommon()
    {
        $this->common();
        $this->character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdV2($this->user_id);
        if (!($this->character)) {
            $message = "EventHorizonRpgBundle:Character:EditCommon user_id=".$this->user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Character entity.');
        }
        $this->form = $this->createForm(new CharacterType(), $this->character);
    }

    public function editAction()
    {
        $this->EditCommon();

        return $this->render('EventHorizonRpgBundle:Character:Edit.html.twig', array(
            'character' => $this->character,
            'form'      => $this->form->createView(),
            'setup'     => $this->setup,
        ));
    }

    public function updateAction()
    {
        $this->EditCommon();

        $this->form->bind($this->request);

        if ($this->form->isValid()) {
            $this->em->getRepository('EventHorizonRpgBundle:Character')->saveEditCharacter($this->character, $this->user);

            return $this->redirect($this->generateUrl('character_show'));
        }

        return $this->render('EventHorizonRpgBundle:Character:Edit.html.twig', array(
            'character' => $this->character,
            'form'      => $this->form->createView(),
            'setup'     => $this->setup,
        ));
    }
}
