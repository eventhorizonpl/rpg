<?php

namespace EventHorizon\RpgBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EventHorizon\RpgBundle\EventHorizonRpgBundle;
use EventHorizon\RpgBundle\Entity\Page;

class PageController extends Controller
{
    public function common()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->request = $this->getRequest();
        $this->request->setLocale($this->get('session')->get('_locale'));
        $this->locale = $this->getRequest()->getLocale();
        $this->request->getSession()->set('referrer', $this->request->getRequestUri());
    }

    public function indexAction()
    {
        $this->common();

        $this->entities = $this->em->getRepository('EventHorizonRpgBundle:Page')->getPagesByLocaleV1($this->locale);

        return $this->render('EventHorizonRpgBundle:Page:index.html.twig', array(
            'entities' => $this->entities,
            'setup'    => $this->setup,
        ));
    }

    public function showAction($name)
    {
        $this->common();

        $this->entity = $this->em->getRepository('EventHorizonRpgBundle:Page')->getPageByLocaleAndNameV1($this->locale, $name);

        if (!($this->entity)) {
            $this->message = "EventHorizonRpgBundle:Page:show locale=".$this->locale." name=".$name;
            EventHorizonRpgBundle::addNotice($this->message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        return $this->render('EventHorizonRpgBundle:Page:show.html.twig', array(
            'entity' => $this->entity,
            'setup'  => $this->setup,
        ));
    }

}
