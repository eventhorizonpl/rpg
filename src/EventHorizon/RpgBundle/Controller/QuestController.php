<?php

namespace EventHorizon\RpgBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use EventHorizon\RpgBundle\EventHorizonRpgBundle;
use EventHorizon\RpgBundle\Entity\Quest;
use EventHorizon\RpgBundle\Form\QuestType;

class QuestController extends Controller
{
    public function common()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->user = $this->container->get('security.context')->getToken()->getUser();
        $this->user_id = $this->user->getId();
        $this->request = $this->getRequest();
        $this->request->setLocale($this->get('session')->get('_locale'));
        $this->locale = $this->request->getLocale();
        $this->request->getSession()->set('referrer', $this->request->getRequestUri());
    }

    public function NewCommon($journal_id)
    {
        $this->common();

        if ($journal_id) {
            $this->journal = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalByIdAndUserIdV1($journal_id, $this->user_id);

            if (!($this->journal)) {
                $message = "EventHorizonRpgBundle:Quest:new id=".$journal_id." user_id=".$this->user_id;
                EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

                throw $this->createNotFoundException('Unable to find Journal entity.');
            }
        } else {
            $this->journal = null;
        }

        $this->character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdV2($this->user_id);
        $this->level = ($this->character->getLevel() + 1);
        $this->rewardExists = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardsByUserIdAndLevelV1($this->user_id, $this->level);

        $this->quest = new Quest();
        $this->form = $this->createForm(new QuestType(array('locale' => $this->locale, 'user' => $this->user)), $this->quest);
    }

    public function newAction($journal_id)
    {
        $this->NewCommon($journal_id);

        if (!($this->rewardExists)) {
            return $this->redirect($this->generateUrl('reward_new'));
        }

        return $this->render('EventHorizonRpgBundle:Quest:New.html.twig', array(
            'form'       => $this->form->createView(),
            'journal_id' => $journal_id,
            'quest'      => $this->quest,
            'setup'      => $this->setup,
            'user'       => $this->user,
        ));
    }

    public function createAction($journal_id)
    {
        $this->NewCommon($journal_id);

        $this->form->bind($this->request);

        if ($this->form->isValid()) {
            $questType = $this->request->request->get('eventhorizon_rpgbundle_questtype');
            $is_visible = $questType['is_visible'];
            $publish_on_facebook = $questType['publish_on_facebook'];
            $this->journal = $this->em->getRepository('EventHorizonRpgBundle:Quest')->saveNewQuest($this->journal, $this->quest, $this->user, $is_visible, $publish_on_facebook);

            $this->publishOnFacebook($is_visible, $publish_on_facebook);

            return $this->redirect($this->generateUrl('journal', array('mode' => 'todo')));
        }

        if (!($this->rewardExists)) {
            return $this->redirect($this->generateUrl('reward_new'));
        }

        return $this->render('EventHorizonRpgBundle:Quest:New.html.twig', array(
            'form'       => $this->form->createView(),
            'journal_id' => $journal_id,
            'quest'      => $this->quest,
            'setup'      => $this->setup,
            'user'       => $this->user,
        ));
    }

    public function EditCommon($id)
    {
        $this->common();
        $this->is_group_quest = 0;
        $this->quest = $this->em->getRepository('EventHorizonRpgBundle:Quest')->getQuestByIdAndUserIdAndIsGroupQuestV1($id, $this->user_id, $this->is_group_quest);

        if (!($this->quest)) {
            $message = "EventHorizonRpgBundle:Quest:EditCommon id=".$id." user_id=".$this->user_id." is_group_quest=".$this->is_group_quest;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Quest entity.');
        }

        $this->journal = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalByQuestIdAndUserIdV1($id, $this->user_id);

        if (!($this->journal)) {
            $message = "EventHorizonRpgBundle:Quest:EditCommon quest_id=".$id." user_id=".$this->user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Journal entity.');
        }

        $this->form = $this->createForm(new QuestType(array('journal' => $this->journal, 'locale' => $this->locale)), $this->quest);
    }

    public function editAction($id)
    {
        $this->EditCommon($id);

        return $this->render('EventHorizonRpgBundle:Quest:Edit.html.twig', array(
            'form'  => $this->form->createView(),
            'quest' => $this->quest,
            'setup' => $this->setup,
            'user'  => $this->user,
        ));
    }

    public function updateAction($id)
    {
        $this->EditCommon($id);

        $this->form->bind($this->request);

        if ($this->form->isValid()) {
            $questType = $this->request->request->get('eventhorizon_rpgbundle_questtype');
            $is_visible = $questType['is_visible'];
            $publish_on_facebook = $questType['publish_on_facebook'];
            $this->em->getRepository('EventHorizonRpgBundle:Quest')->saveEditQuest($this->journal, $this->quest, $this->user, $is_visible, $publish_on_facebook);

            $this->publishOnFacebook($is_visible, $publish_on_facebook);

            return $this->redirect($this->generateUrl('journal', array('mode' => 'todo')));
        }

        return $this->render('EventHorizonRpgBundle:Quest:Edit.html.twig', array(
            'form'  => $this->form->createView(),
            'quest' => $this->quest,
            'setup' => $this->setup,
            'user'  => $this->user,
        ));
    }

    public function publishOnFacebook($is_visible, $publish_on_facebook)
    {
        if (($is_visible) and ($publish_on_facebook) and ($this->user->getFacebookId()) and ($this->user->getCharacter()->getIsVisible())) {
            $facebookSdk = $this->get("armetiz.facebook.rpg");
            $accessToken = $facebookSdk->getAccessToken();
            $facebookSdk->setAccessToken($accessToken);
            $link = $this->generateUrl('show_journal', array('journal_id' => $this->journal->getId(), 'user_id' => $this->user_id), true);
            $messge = $this->user->getCharacter()->getName()." ".$this->get('translator')->trans('podejmuje nowe wyzwanie');

            try {
                $facebookSdk->api('/me/feed', 'POST', array(
                    'link' => $link,
                    'message' => $messge
                ));
            } catch (\Exception $e) {
                $this->get('session')->setFlash('error', $this->get('translator')->trans('Brak uprawnień do publikacji na Facebooku'));
            }
        }
    }
}
