<?php

namespace EventHorizon\RpgBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EventHorizon\RpgBundle\EventHorizonRpgBundle;

/**
 * @Route("/show")
 */
class ShowController extends Controller
{
    public function common()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->request = $this->getRequest();
        $this->request->setLocale($this->get('session')->get('_locale'));
        $this->request->getSession()->set('referrer', $this->request->getRequestUri());
        $this->is_visible = 1;
    }

    /**
     * @Method("get")
     * @Route("/statistics", name="show_statistics")
     * @Template("EventHorizonRpgBundle:Show:StatisticsShow.html.twig")
     */
    public function StatisticsShowAction()
    {
        $this->common();

        $journals = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalsV1();

        $journal_count = count($journals);
        $journal_is_completed_count = 0;
        $journal_is_hidden_count = 0;
        $journal_is_visible_count = 0;
        $journal_waiting_ftc_count = 0;

        foreach ($journals as $journal) {
            if ($journal->getIsCompleted()) {
                $journal_is_completed_count += 1;
            }
            if ($journal->getIsHidden()) {
                $journal_is_hidden_count += 1;
            }
            if ($journal->getIsVisible()) {
                $journal_is_visible_count += 1;
            }
            if ((!($journal->getIsCompleted())) and (!($journal->getIsHidden()))) {
                $journal_waiting_ftc_count += 1;
            }
        }

        return array(
            'journal_count'              => $journal_count,
            'journal_is_completed_count' => $journal_is_completed_count,
            'journal_is_hidden_count'    => $journal_is_hidden_count,
            'journal_is_visible_count'   => $journal_is_visible_count,
            'journal_waiting_ftc_count'  => $journal_waiting_ftc_count,
            'setup'                      => $this->setup,
        );
    }

    /**
     * @Method("get")
     * @Route("/characters", name="show_characters")
     * @Template("EventHorizonRpgBundle:Show:CharactersList.html.twig")
     */
    public function CharactersListAction()
    {
        $this->common();

        $characters = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharactersByIsVisibleV1($this->is_visible);

        return array(
            'characters' => $characters,
            'setup'      => $this->setup,
        );
    }

    /**
     * @Method("get")
     * @Route("/{user_id}/character", name="show_character", requirements={"user_id" = "\d+"})
     * @Template("EventHorizonRpgBundle:Show:CharacterShow.html.twig")
     */
    public function CharacterShowAction($user_id)
    {
        $this->common();

        $character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdAndIsVisibleV1($user_id, $this->is_visible);
        if (!($character)) {
            $message = "EventHorizonRpgBundle:Show:CharacterShow user_id=".$user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Character entity.');
        }

        $experience = $character->getExperience();
        $level = $character->getLevel();

        $promotion_level = EventHorizonRpgBundle::getPromotionLevel();
        $progress = ((($experience / $promotion_level) - ($level - 1)) * 100);
        $required_experience = ($level * $promotion_level);

        return array(
            'character'           => $character,
            'setup'               => $this->setup,
            'progress'            => $progress,
            'required_experience' => $required_experience,
            'user_id'             => $user_id,
        );
    }

    /**
     * @Method("get")
     * @Route("/{user_id}/journal/{journal_id}", name="show_journal", requirements={"user_id" = "\d+", "journal_id" = "\d+"})
     * @Template("EventHorizonRpgBundle:Show:JournalShow.html.twig")
     */
    public function JournalShowAction($user_id, $journal_id)
    {
        $this->common();

        $character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdAndIsVisibleV1($user_id, $this->is_visible);
        if (!($character)) {
            $message = "EventHorizonRpgBundle:Show:JournalShow user_id=".$user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Character entity.');
        }

        $journal = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalByIdAndUserIdAndIsVisibleV1($journal_id, $user_id, $this->is_visible);
        if (!($journal)) {
            $message = "EventHorizonRpgBundle:Show:JournalShow id=".$journal_id." user_id=".$user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Journal entity.');
        }

        return array(
            'character' => $character,
            'journal'   => $journal,
            'setup'     => $this->setup,
            'user_id'   => $user_id,
        );
    }

    /**
     * @Method("get")
     * @Route("/{user_id}/journal/{mode}/{type}", defaults={"mode" = "todo", "type" = "all"}, name="show_journals", requirements={"user_id" = "\d+"})
     * @Template("EventHorizonRpgBundle:Show:JournalsList.html.twig")
     */
    public function JournalsListAction($user_id, $mode, $type)
    {
        $this->common();

        $character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdAndIsVisibleV1($user_id, $this->is_visible);
        if (!($character)) {
            $message = "EventHorizonRpgBundle:Show:JournalsList user_id=".$user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Character entity.');
        }

        if ($mode == 'todo') {
            $is_completed = 0;
        } else {
            $is_completed = 1;
        }
        if ($type == 'all') {
            $journals = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalsByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $this->is_visible);
        } else {
            $journals = $this->em->getRepository('EventHorizonRpgBundle:Journal')->getJournalsByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $this->is_visible);
        }

        return array(
            'character'    => $character,
            'is_completed' => $is_completed,
            'journals'     => $journals,
            'mode'         => $mode,
            'setup'        => $this->setup,
            'type'         => $type,
            'user_id'      => $user_id,
        );
    }

    /**
     * @Method("get")
     * @Route("/{user_id}/reward/{reward_id}", name="show_reward", requirements={"user_id" = "\d+", "reward_id" = "\d+"})
     * @Template("EventHorizonRpgBundle:Show:RewardShow.html.twig")
     */
    public function RewardShowAction($user_id, $reward_id)
    {
        $this->common();

        $character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdAndIsVisibleV1($user_id, $this->is_visible);
        if (!($character)) {
            $message = "EventHorizonRpgBundle:Show:RewardShow user_id=".$user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Character entity.');
        }

        $reward = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardByIdAndUserIdAndIsVisibleV1($reward_id, $user_id, $this->is_visible);
        if (!($reward)) {
            $message = "EventHorizonRpgBundle:Show:RewardShow reward_id=".$reward_id." user_id=".$user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Reward entity.');
        }

        return array(
            'character' => $character,
            'reward'    => $reward,
            'setup'     => $this->setup,
            'user_id'   => $user_id,
        );
    }

    /**
     * @Method("get")
     * @Route("/{user_id}/reward", name="show_rewards", requirements={"user_id" = "\d+"})
     * @Template("EventHorizonRpgBundle:Show:RewardsList.html.twig")
     */
    public function RewardsListAction($user_id)
    {
        $this->common();

        $character = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdAndIsVisibleV1($user_id, $this->is_visible);
        if (!($character)) {
            $message = "EventHorizonRpgBundle:Show:RewardsList user_id=".$user_id;
            EventHorizonRpgBundle::addNotice($message, $this->get('event_horizon_log'), $this->get('request'));

            throw $this->createNotFoundException('Unable to find Character entity.');
        }

        $rewards = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardsByUserIdAndIsVisibleV1($user_id, $this->is_visible);

        return array(
            'character' => $character,
            'rewards'   => $rewards,
            'setup'     => $this->setup,
            'user_id'   => $user_id,
        );
    }
}
