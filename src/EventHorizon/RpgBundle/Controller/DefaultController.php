<?php

namespace EventHorizon\RpgBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function common()
    {
        $this->em = $this->getDoctrine()->getManager();
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->request = $this->getRequest();
        $this->request->setLocale($this->get('session')->get('_locale'));
        $this->request->getSession()->set('referrer', $this->request->getRequestUri());
    }

    public function indexAction()
    {
        $this->common();

        if (!($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))) {
            $this->register_form = $this->container->get('fos_user.registration.form');

            $this->login_form_csrf_token = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');

            return $this->render('EventHorizonRpgBundle:Default:index.html.twig', array(
                'login_form_csrf_token' => $this->login_form_csrf_token,
                'register_form'         => $this->register_form->createView(),
                'setup'                 => $this->setup,
            ));
        } else {
            return $this->render('EventHorizonRpgBundle:Default:index.html.twig', array(
                'setup' => $this->setup,
            ));
        }
    }

    public function languageAction($language)
    {
        $this->get('session')->set('_locale', $language);

        $referer = $this->get('session')->get('referrer');

        if ($referer) {
            return $this->redirect($referer);
        } else {
            return $this->redirect($this->generateUrl('_homepage'));
        }
    }

    public function layoutAction()
    {
        $this->common();

        return $this->render('EventHorizonRpgBundle:Default:layout.html.twig', array(
            'setup' => $this->setup,
        ));
    }

}
