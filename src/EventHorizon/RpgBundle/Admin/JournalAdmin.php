<?php

namespace EventHorizon\RpgBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class JournalAdmin extends Admin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('character')
            ->add('quest')
            ->add('user')
            ->add('is_completed')
            ->add('is_hidden')
            ->add('is_visible')
            ->add('completed_at')
            ->add('created_at')
            ->add('updated_at')
        ;
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('edit');
    }
}
