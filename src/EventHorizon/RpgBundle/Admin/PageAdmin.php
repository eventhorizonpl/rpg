<?php

namespace EventHorizon\RpgBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use EventHorizon\RpgBundle\Form\Type\BooleanType;
use EventHorizon\RpgBundle\Form\Type\PageLocaleType;
use EventHorizon\RpgBundle\Form\Type\PageTypeType;

class PageAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('content')
            ->add('locale', new PageLocaleType())
            ->add('name')
            ->add('title')
            ->add('type', new PageTypeType())
            ->add('is_visible', new BooleanType())
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('content')
            ->add('locale')
            ->add('name')
            ->add('title')
            ->add('type')
            ->add('is_visible')
            ->add('created_at')
            ->add('updated_at')
        ;
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }
}
