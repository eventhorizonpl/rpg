<?php

namespace EventHorizon\RpgBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class CharacterAdmin extends Admin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('user')
            ->add('attribute_agility')
            ->add('attribute_charisma')
            ->add('attribute_endurance')
            ->add('attribute_intelligence')
            ->add('attribute_luck')
            ->add('attribute_perception')
            ->add('attribute_strength')
            ->add('attribute_willpower')
            ->add('attribute_wisdom')
            ->add('avatar')
            ->add('birth_date')
            ->add('experience')
            ->add('gender')
            ->add('level')
            ->add('name')
            ->add('skill_art')
            ->add('skill_culture')
            ->add('skill_entertainment')
            ->add('skill_helpfulness')
            ->add('skill_home')
            ->add('skill_learning')
            ->add('skill_recreation')
            ->add('skill_sport')
            ->add('skill_work')
            ->add('is_visible')
            ->add('created_at')
            ->add('updated_at')
        ;
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('edit');
    }
}
