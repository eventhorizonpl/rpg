<?php

namespace EventHorizon\RpgBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use EventHorizon\RpgBundle\Form\Type\BooleanType;
use EventHorizon\RpgBundle\Form\Type\SetupThemeType;

class SetupAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('copyright_holder')
            ->add('copyright_holder_email')
            ->add('domain')
            ->add('game_contact_email')
            ->add('game_name')
            ->add('statistics_enabled', new BooleanType())
            ->add('statistics_key')
            ->add('test_mode', new BooleanType())
            ->add('theme', new SetupThemeType())
            ->add('url')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('copyright_holder')
            ->add('copyright_holder_email')
            ->add('domain')
            ->add('game_contact_email')
            ->add('game_name')
            ->add('statistics_enabled')
            ->add('statistics_key')
            ->add('test_mode')
            ->add('theme')
            ->add('url')
            ->add('created_at')
            ->add('updated_at')
        ;
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }

    public function postUpdate($setup)
    {
        $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository('EventHorizonRpgBundle:Setup')->deleteSetupResultCacheV1();
    }
}
