<?php

namespace EventHorizon\RpgBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class QuestAdmin extends Admin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('character')
            ->add('journal')
            ->add('user')
            ->add('attribute_reward')
            ->add('content')
            ->add('skill_reward')
            ->add('title')
            ->add('is_blocked')
            ->add('is_group_quest')
            ->add('created_at')
            ->add('updated_at')
        ;
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->remove('edit');
    }
}
