<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\RpgBundle\DataFixtures\ORM\Conf;
use EventHorizon\RpgBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('admin@getactive.pl');
        $user->setEnabled(true);
        $user->setPlainPassword('rpg');
        $user->setRoles(array('ROLE_ADMIN', 'ROLE_ADVERTISEMENT_MANAGER', 'ROLE_ADVERTISEMENT_OPERATOR', 'ROLE_SUPER_ADMIN', 'ROLE_USER'));
        $user->setSuperAdmin(true);
        $user->setUsername('admin');
        //$user->setCreatedAt(new \DateTime());
        //$user->setUpdatedAt(new \DateTime());

        $manager->persist($user);
        $manager->flush();

        $this->addReference('admin-user', $user);

        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$users; $i++) {
                $user = new User();
                $user->setEmail('test'.$i.'@test'.$i.'.pl');
                $user->setEnabled(true);
                $user->setPlainPassword('test'.$i);
                $user->setRoles(array('ROLE_PLAYER', 'ROLE_USER'));
                $user->setUsername('test'.$i);
                //$user->setCreatedAt(new \DateTime());
                //$user->setUpdatedAt(new \DateTime());

                $manager->persist($user);
                $manager->flush();

                $this->addReference('user'.$i, $user);
            }
        }
    }

    public function getOrder()
    {
        return 2;
    }
}
