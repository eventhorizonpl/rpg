<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

class Conf
{
    public static $all_fixtures = 1;
    public static $users = 5;
    public static $quests = 5;
    public static $rewards = 5;
}
