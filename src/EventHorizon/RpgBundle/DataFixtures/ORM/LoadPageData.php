<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\RpgBundle\Entity\Page;

class LoadPageData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $page = new Page();
        $page->setContent('Pomoc');
        $page->setLocale('pl');
        $page->setName('help');
        $page->setTitle('Pomoc');
        $page->setType('normal');
        $page->setIsVisible(1);
        //$page->setCreatedAt(new \DateTime());
        //$page->setUpdatedAt(new \DateTime());

        $manager->persist($page);
        $manager->flush();

        $page = new Page();
        $page->setContent('Help');
        $page->setLocale('en');
        $page->setName('help');
        $page->setTitle('Help');
        $page->setType('normal');
        $page->setIsVisible(1);
        //$page->setCreatedAt(new \DateTime());
        //$page->setUpdatedAt(new \DateTime());

        $manager->persist($page);
        $manager->flush();

        $page = new Page();
        $page->setContent('Partnerzy medialni');
        $page->setLocale('pl');
        $page->setName('media_partners');
        $page->setTitle('Partnerzy medialni');
        $page->setType('normal');
        $page->setIsVisible(1);
        //$page->setCreatedAt(new \DateTime());
        //$page->setUpdatedAt(new \DateTime());

        $manager->persist($page);
        $manager->flush();

        $page = new Page();
        $page->setContent('Media partners');
        $page->setLocale('en');
        $page->setName('media_partners');
        $page->setTitle('Media partners');
        $page->setType('normal');
        $page->setIsVisible(1);
        //$page->setCreatedAt(new \DateTime());
        //$page->setUpdatedAt(new \DateTime());

        $manager->persist($page);
        $manager->flush();

        $page = new Page();
        $page->setContent('Regulamin');
        $page->setLocale('pl');
        $page->setName('regulations');
        $page->setTitle('Regulamin');
        $page->setType('normal');
        $page->setIsVisible(1);
        //$page->setCreatedAt(new \DateTime());
        //$page->setUpdatedAt(new \DateTime());

        $manager->persist($page);
        $manager->flush();

        $page = new Page();
        $page->setContent('Regulations');
        $page->setLocale('en');
        $page->setName('regulations');
        $page->setTitle('Regulations');
        $page->setType('normal');
        $page->setIsVisible(1);
        //$page->setCreatedAt(new \DateTime());
        //$page->setUpdatedAt(new \DateTime());

        $manager->persist($page);
        $manager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}
