<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\RpgBundle\Entity\Account;

class LoadAccountData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $account = new Account();
        $account->setUser($manager->merge($this->getReference('admin-user')));
        $account->setType("admin");
        $account->setExpiresAt(new \DateTime());
        //$account->setCreatedAt(new \DateTime());
        //$account->setUpdatedAt(new \DateTime());

        $manager->persist($account);
        $manager->flush();

        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$users; $i++) {
                $account = new Account();
                $account->setUser($manager->merge($this->getReference('user'.$i)));
                $account->setType("player");
                $account->setExpiresAt(new \DateTime());
                //$account->setCreatedAt(new \DateTime());
                //$account->setUpdatedAt(new \DateTime());

                $manager->persist($account);
                $manager->flush();
            }
        }
    }

    public function getOrder()
    {
        return 5;
    }
}
