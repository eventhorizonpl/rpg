<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\RpgBundle\DataFixtures\ORM\Conf;
use EventHorizon\RpgBundle\Entity\Quest;

class LoadQuestData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$users; $i++) {
                for ($j = 1; $j <= Conf::$quests; $j++) {
                    $quest = new Quest();
                    $quest->setCharacter($manager->merge($this->getReference('character'.$i)));
                    $quest->setUser($manager->merge($this->getReference('user'.$i)));
                    $quest->setAttributeReward('agility');
                    $quest->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum augue vel lacus pellentesque venenatis. Proin a mattis est. Donec justo urna, hendrerit vel volutpat et, suscipit eu ligula. Morbi a purus sit amet nibh semper pretium vitae sit amet sapien. Vestibulum viverra, urna id consequat sagittis, est enim tempus massa, vel lobortis augue odio eget tortor. Curabitur facilisis tempor enim, et semper risus laoreet in. Proin tincidunt interdum sapien quis egestas. Nulla interdum tempus tempor. Sed sit amet velit arcu. Suspendisse et lacus at est gravida pellentesque. Integer erat metus, mollis sit amet egestas at, dictum ut nunc.'.$i.'_'.$j);
                    $quest->setSkillReward('sport');
                    $quest->setTitle('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum augue vel lacus pellentesque venenatis.'.$i.'_'.$j);
                    $quest->setType('normal');
                    //$quest->setCreatedAt(new \DateTime());
                    //$quest->setUpdatedAt(new \DateTime());

                    $manager->persist($quest);
                    $manager->flush();

                    $this->addReference('quest'.$i.'_'.$j, $quest);
                }
            }
        }
    }

    public function getOrder()
    {
        return 7;
    }
}
