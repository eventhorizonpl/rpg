<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\RpgBundle\DataFixtures\ORM\Conf;
use EventHorizon\RpgBundle\Entity\Character;

class LoadCharacterData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $character = new Character();
        $character->setUser($manager->merge($this->getReference('admin-user')));
        $character->setAttributeAgility(10);
        $character->setAttributeCharisma(10);
        $character->setAttributeEndurance(10);
        $character->setAttributeIntelligence(10);
        $character->setAttributeLuck(10);
        $character->setAttributePerception(10);
        $character->setAttributeStrength(10);
        $character->setAttributeWillpower(10);
        $character->setAttributeWisdom(10);
        $character->setGender('male');
        $character->setLevel(1);
        $character->setName('Administrator');
        $character->setSkillArt(10);
        $character->setSkillCulture(10);
        $character->setSkillEntertainment(10);
        $character->setSkillHelpfulness(10);
        $character->setSkillHome(10);
        $character->setSkillLearning(10);
        $character->setSkillRecreation(10);
        $character->setSkillSport(10);
        $character->setSkillWork(10);
        $character->setIsVisible(0);
        //$character->setCreatedAt(new \DateTime());
        //$character->setUpdatedAt(new \DateTime());

        $manager->persist($character);
        $manager->flush();

        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$users; $i++) {
                $character = new Character();
                $character->setUser($manager->merge($this->getReference('user'.$i)));
                $character->setAttributeAgility(10);
                $character->setAttributeCharisma(10);
                $character->setAttributeEndurance(10);
                $character->setAttributeIntelligence(10);
                $character->setAttributeLuck(10);
                $character->setAttributePerception(10);
                $character->setAttributeStrength(10);
                $character->setAttributeWillpower(10);
                $character->setAttributeWisdom(10);
                $character->setGender('male');
                $character->setLevel(1);
                $character->setName('test'.$i);
                $character->setSkillArt(10);
                $character->setSkillCulture(10);
                $character->setSkillEntertainment(10);
                $character->setSkillHelpfulness(10);
                $character->setSkillHome(10);
                $character->setSkillLearning(10);
                $character->setSkillRecreation(10);
                $character->setSkillSport(10);
                $character->setSkillWork(10);
                $character->setIsVisible(0);
                //$character->setCreatedAt(new \DateTime());
                //$character->setUpdatedAt(new \DateTime());

                $manager->persist($character);
                $manager->flush();

                $this->addReference('character'.$i, $character);
            }
        }
    }

    public function getOrder()
    {
        return 6;
    }
}
