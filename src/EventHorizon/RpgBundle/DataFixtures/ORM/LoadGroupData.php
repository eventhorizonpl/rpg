<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\RpgBundle\Entity\Group;

class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $adminGroup = new Group('admin', array('ROLE_ADMIN', 'ROLE_ADVERTISEMENT_MANAGER', 'ROLE_ADVERTISEMENT_OPERATOR', 'ROLE_SUPER_ADMIN', 'ROLE_USER'));
        //$adminGroup->setCreatedAt(new \DateTime());
        //$adminGroup->setUpdatedAt(new \DateTime());

        $manager->persist($adminGroup);
        $manager->flush();

        $this->addReference('admin-group', $adminGroup);

        $advertisementManagerGroup = new Group('advertisement_manager', array('ROLE_ADVERTISEMENT_MANAGER', 'ROLE_USER'));
        //$advertisementManagerGroup->setCreatedAt(new \DateTime());
        //$advertisementManagerGroup->setUpdatedAt(new \DateTime());

        $manager->persist($advertisementManagerGroup);
        $manager->flush();

        $this->addReference('advertisement_manager-group', $advertisementManagerGroup);

        $advertisementOperatorGroup = new Group('advertisement_operator', array('ROLE_ADVERTISEMENT_OPERATOR', 'ROLE_USER'));
        //$advertisementOperatorGroup->setCreatedAt(new \DateTime());
        //$advertisementOperatorGroup->setUpdatedAt(new \DateTime());

        $manager->persist($advertisementOperatorGroup);
        $manager->flush();

        $this->addReference('advertisement_operator-group', $advertisementOperatorGroup);

        $playerGroup = new Group('player', array('ROLE_PLAYER', 'ROLE_USER'));
        //$playerGroup->setCreatedAt(new \DateTime());
        //$playerGroup->setUpdatedAt(new \DateTime());

        $manager->persist($playerGroup);
        $manager->flush();

        $this->addReference('player-group', $playerGroup);
    }

    public function getOrder()
    {
        return 3;
    }
}
