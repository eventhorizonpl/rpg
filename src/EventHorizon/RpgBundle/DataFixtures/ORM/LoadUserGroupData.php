<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\RpgBundle\DataFixtures\ORM\Conf;
use EventHorizon\RpgBundle\Entity\UserGroup;

class LoadUserGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user_group = new UserGroup();
        $user_group->setUser($manager->merge($this->getReference('admin-user')));
        $user_group->setGroup($manager->merge($this->getReference('admin-group')));
        //$user_group->setCreatedAt(new \DateTime());
        //$user_group->setUpdatedAt(new \DateTime());

        $manager->persist($user_group);
        $manager->flush();

        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$users; $i++) {
                $user_group = new UserGroup();
                $user_group->setUser($manager->merge($this->getReference('user'.$i)));
                $user_group->setGroup($manager->merge($this->getReference('player-group')));
                //$user_group->setCreatedAt(new \DateTime());
                //$user_group->setUpdatedAt(new \DateTime());

                $manager->persist($user_group);
                $manager->flush();
            }
        }
    }

    public function getOrder()
    {
        return 4;
    }
}
