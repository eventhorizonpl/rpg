<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\RpgBundle\DataFixtures\ORM\Conf;
use EventHorizon\RpgBundle\Entity\Journal;

class LoadJournalData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        if (Conf::$all_fixtures) {
            for ($i = 1; $i <= Conf::$users; $i++) {
                for ($j = 1; $j <= Conf::$quests; $j++) {
                    $journal = new Journal();
                    $journal->setCharacter($manager->merge($this->getReference('character'.$i)));
                    $journal->setQuest($manager->merge($this->getReference('quest'.$i.'_'.$j)));
                    $journal->setUser($manager->merge($this->getReference('user'.$i)));
                    //$journal->setCreatedAt(new \DateTime());
                    //$journal->setUpdatedAt(new \DateTime());

                    $manager->persist($journal);
                    $manager->flush();
                }
            }
        }
    }

    public function getOrder()
    {
        return 8;
    }
}
