<?php

namespace EventHorizon\RpgBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\RpgBundle\Entity\Setup;

class LoadSetupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $setup = new Setup();
        $setup->setCopyrightHolder('Event Horizon');
        $setup->setCopyrightHolderEmail('michal@eventhorizon.pl');
        $setup->setDomain('getactive.pl');
        $setup->setGameContactEmail('michal@eventhorizon.pl');
        $setup->setGameName('getACTIVE!');
        $setup->setTheme('default');
        $setup->setUrl('http://getactive.pl/');
        //$setup->setCreatedAt(new \DateTime());
        //$setup->setUpdatedAt(new \DateTime());

        $manager->persist($setup);
        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
