<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;
use EventHorizon\RpgBundle\Entity\Journal;
use EventHorizon\RpgBundle\Entity\Quest;

class QuestRepository extends EntityRepository
{
    public function QuestSelectByIdAndUserIdAndIsGroupQuestV1($quest_id, $user_id, $is_group_quest)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT q FROM EventHorizonRpgBundle:Quest q JOIN q.journal j WHERE q.id = :id AND q.user = :user_id AND q.is_group_quest = :is_group_quest AND q.is_blocked = :is_blocked AND j.is_completed = :is_completed')
            ->setParameter('id', $quest_id)
            ->setParameter('user_id', $user_id)
            ->setParameter('is_group_quest', $is_group_quest)
            ->setParameter('is_blocked', 0)
            ->setParameter('is_completed', 0);

        return $query;
    }

    public function getQuestByIdAndUserIdAndIsGroupQuestV1($quest_id, $user_id, $is_group_quest)
    {
        $query = $this->QuestSelectByIdAndUserIdAndIsGroupQuestV1($quest_id, $user_id, $is_group_quest);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function saveNewQuest($parent_journal, \EventHorizon\RpgBundle\Entity\Quest $quest, \EventHorizon\RpgBundle\Entity\User $user, $is_visible, $publish_on_facebook)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $user_id = $user->getId();
            $character = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdV1($user_id);

            $quest->setCharacter($character);
            $quest->setUser($user);
            $quest->setCreatedAt(new \DateTime());
            $quest->setUpdatedAt(new \DateTime());
            $this->getEntityManager()->persist($quest);
            $this->getEntityManager()->flush();

            $journal = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->addJournal($character, $parent_journal, $quest, $user, $is_visible, $publish_on_facebook);

            $is_completed = 0;
            $is_visible = 1;
            if ($parent_journal) {
                $parent_journal_id = $parent_journal->getId();
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByIdAndUserIdV1($parent_journal_id, $user_id);
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByIdAndUserIdAndIsVisibleV1($parent_journal_id, $user_id, $is_visible);
            }
            $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndIsCompletedV1($user_id, $is_completed);
            $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $is_visible);
            foreach (array('dream', 'standard', 'training') as $type) {
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedV1($user_id, $type, $is_completed);
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $is_visible);
            }

            $this->getEntityManager()->getConnection()->commit();

            return $journal;
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }

    public function saveEditQuest(\EventHorizon\RpgBundle\Entity\Journal $journal, \EventHorizon\RpgBundle\Entity\Quest $quest, \EventHorizon\RpgBundle\Entity\User $user, $is_visible, $publish_on_facebook)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $user_id = $user->getId();

            $this->getEntityManager()->persist($quest);

            $journal->setIsVisible($is_visible);
            $journal->setPublishOnFacebook($publish_on_facebook);
            $this->getEntityManager()->persist($journal);
            $this->getEntityManager()->flush();

            $is_completed = 0;
            $is_visible = 1;
            $journal_id = $journal->getId();
            $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByIdAndUserIdV1($journal_id, $user_id);
            $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByIdAndUserIdAndIsVisibleV1($journal_id, $user_id, $is_visible);
            $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndIsCompletedV1($user_id, $is_completed);
            $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $is_visible);
            foreach (array('dream', 'standard', 'training') as $type) {
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedV1($user_id, $type, $is_completed);
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $is_visible);
            }

            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }
}
