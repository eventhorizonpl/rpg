<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;
use EventHorizon\RpgBundle\Entity\Account;
use EventHorizon\RpgBundle\Entity\Character;
use EventHorizon\RpgBundle\Entity\UserGroup;

class UserRepository extends EntityRepository
{
    public function UserSelectV1()
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT u FROM EventHorizonRpgBundle:User u');

        return $query;
    }

    public function getUsersV1()
    {
        $query = $this->UserSelectV1();

        return $query->getResult();
    }

    public function createPlayerObjects(\EventHorizon\RpgBundle\Entity\User $user)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $account = new Account();
            $account->setUser($user);
            $account->setType("player");
            $account->setExpiresAt(new \DateTime('2100-01-01'));
            $account->setCreatedAt(new \DateTime());
            $account->setUpdatedAt(new \DateTime());
            $this->getEntityManager()->persist($account);

            $name = 'player';
            $group = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Group')->getGroupByNameV1($name);
            $user_group = new UserGroup();
            $user_group->setUser($user);
            $user_group->setGroup($group);
            $user_group->setCreatedAt(new \DateTime());
            $user_group->setUpdatedAt(new \DateTime());
            $this->getEntityManager()->persist($user_group);

            $character = new Character();
            $character->setUser($user);
            $character->setAttributeAgility(10);
            $character->setAttributeCharisma(10);
            $character->setAttributeEndurance(10);
            $character->setAttributeIntelligence(10);
            $character->setAttributeLuck(10);
            $character->setAttributePerception(10);
            $character->setAttributeStrength(10);
            $character->setAttributeWillpower(10);
            $character->setAttributeWisdom(10);
            $character->setLevel(1);
            $character->setSkillArt(10);
            $character->setSkillCulture(10);
            $character->setSkillEntertainment(10);
            $character->setSkillHelpfulness(10);
            $character->setSkillHome(10);
            $character->setSkillLearning(10);
            $character->setSkillRecreation(10);
            $character->setSkillSport(10);
            $character->setSkillWork(10);
            $character->setIsVisible(0);
            $character->setCreatedAt(new \DateTime());
            $character->setUpdatedAt(new \DateTime());
            $this->getEntityManager()->persist($character);
            $this->getEntityManager()->flush();

            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }
}
