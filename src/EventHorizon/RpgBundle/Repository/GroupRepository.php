<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;
use EventHorizon\RpgBundle\Cache;

class GroupRepository extends EntityRepository
{
    public function GroupSelectByNameV1($name)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT g FROM EventHorizonRpgBundle:Group g WHERE g.name = :name')
            ->setParameter('name', $name);

        return $query;
    }

    public function GroupResultCacheByNameV1($query, $name)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_group_name_'.$name.'_v1');

        return $query;
    }

    public function getGroupByNameV1($name)
    {
        $query = $this->GroupSelectByNameV1($name);
        $query = $this->GroupResultCacheByNameV1($query, $name);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
