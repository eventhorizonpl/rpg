<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;
use EventHorizon\RpgBundle\Cache;

class CharacterRepository extends EntityRepository
{
    public function CharacterSelectV1()
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT c FROM EventHorizonRpgBundle:Character c');

        return $query;
    }

    public function CharacterSelectByUserIdV1($user_id)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT c FROM EventHorizonRpgBundle:Character c WHERE c.user = :user_id')
            ->setParameter('user_id', $user_id);

        return $query;
    }

    public function CharacterSelectByUserIdAndIsVisibleV1($user_id, $is_visible)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT c FROM EventHorizonRpgBundle:Character c WHERE c.user = :user_id AND c.is_visible = :is_visible')
            ->setParameter('user_id', $user_id)
            ->setParameter('is_visible', $is_visible);

        return $query;
    }

    public function CharacterSelectByIsVisibleV1($is_visible)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT c FROM EventHorizonRpgBundle:Character c WHERE c.is_visible = :is_visible ORDER BY c.level DESC')
            ->setParameter('is_visible', $is_visible);

        return $query;
    }

    public function CharacterResultCacheByUserIdV1($query, $user_id)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_character_user_id_'.$user_id.'_v1');

        return $query;
    }

    public function CharacterResultCacheByUserIdAndIsVisibleV1($query, $user_id, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_character_user_id_'.$user_id.'_is_visible_'.$is_visible.'_v1');

        return $query;
    }

    public function CharacterResultCacheByIsVisibleV1($query, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_character_is_visible_'.$is_visible.'_v1');

        return $query;
    }

    public function deleteCharacterResultCacheByUserIdV1($user_id)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_character_user_id_'.$user_id.'_v1');
    }

    public function deleteCharacterResultCacheByUserIdAndIsVisibleV1($user_id, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_character_user_id_'.$user_id.'_is_visible_'.$is_visible.'_v1');
    }

    public function deleteCharacterResultCacheByIsVisibleV1($is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_character_is_visible_'.$is_visible.'_v1');
    }

    public function getCharacterByUserIdV1($user_id)
    {
        $query = $this->CharacterSelectByUserIdV1($user_id);
        $query = $this->CharacterResultCacheByUserIdV1($query, $user_id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getCharacterByUserIdV2($user_id)
    {
        $query = $this->CharacterSelectByUserIdV1($user_id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getCharacterByUserIdAndIsVisibleV1($user_id, $is_visible)
    {
        $query = $this->CharacterSelectByUserIdAndIsVisibleV1($user_id, $is_visible);
        $query = $this->CharacterResultCacheByUserIdAndIsVisibleV1($query, $user_id, $is_visible);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getCharactersV1()
    {
        $query = $this->CharacterSelectV1();

        return $query->getResult();
    }

    public function getCharactersByIsVisibleV1($is_visible)
    {
        $query = $this->CharacterSelectByIsVisibleV1($is_visible);
        $query = $this->CharacterResultCacheByIsVisibleV1($query, $is_visible);

        return $query->getResult();
    }

    public function saveEditCharacter(\EventHorizon\RpgBundle\Entity\Character $character, \EventHorizon\RpgBundle\Entity\User $user)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $user_id = $user->getId();
            $is_visible = 1;

            $this->getEntityManager()->persist($character);
            $this->getEntityManager()->flush();

            $result_cache = $this->deleteCharacterResultCacheByUserIdV1($user_id);
            $result_cache = $this->deleteCharacterResultCacheByUserIdAndIsVisibleV1($user_id, $is_visible);
            $result_cache = $this->deleteCharacterResultCacheByIsVisibleV1($is_visible);

            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }
}
