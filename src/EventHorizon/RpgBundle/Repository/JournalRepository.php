<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;
use EventHorizon\RpgBundle\Cache;
use EventHorizon\RpgBundle\Entity\Journal;
use EventHorizon\RpgBundle\EventHorizonRpgBundle;

class JournalRepository extends EntityRepository
{
    public function addJournal(\EventHorizon\RpgBundle\Entity\Character $character, $parent_journal, \EventHorizon\RpgBundle\Entity\Quest $quest, \EventHorizon\RpgBundle\Entity\User $user, $is_visible, $publish_on_facebook)
    {
        $journal = new Journal();
        $journal->setCharacter($character);

        if ($parent_journal) {
            $journal->setJournal($parent_journal);
            $parent_journal->setQuestCounter($parent_journal->getQuestCounter() + 1);
            $this->getEntityManager()->persist($parent_journal);
        }

        $journal->setQuest($quest);
        $journal->setUser($user);
        $journal->setIsVisible($is_visible);
        $journal->setPublishOnFacebook($publish_on_facebook);
        $journal->setCreatedAt(new \DateTime());
        $journal->setUpdatedAt(new \DateTime());

        $this->getEntityManager()->persist($journal);
        $this->getEntityManager()->flush();

        return $journal;
    }

    public function JournalSelectV1()
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT j, q FROM EventHorizonRpgBundle:Journal j JOIN j.quest q ORDER BY j.created_at DESC');

        return $query;
    }

    public function JournalSelectByIdAndUserIdV1($journal_id, $user_id)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT j, jj, jjq, jq, js, jsq FROM EventHorizonRpgBundle:Journal j LEFT JOIN j.journal jj LEFT JOIN jj.quest jjq JOIN j.quest jq LEFT JOIN j.subquest js LEFT JOIN js.quest jsq WHERE j.id = :journal_id AND j.user = :user_id AND jq.is_blocked = :is_blocked')
            ->setParameter('journal_id', $journal_id)
            ->setParameter('user_id', $user_id)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function JournalSelectByIdAndUserIdAndIsCompletedV1($journal_id, $user_id, $is_completed)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT j, q FROM EventHorizonRpgBundle:Journal j JOIN j.quest q WHERE j.id = :journal_id AND j.user = :user_id AND j.is_completed = :is_completed AND q.is_blocked = :is_blocked')
            ->setParameter('journal_id', $journal_id)
            ->setParameter('user_id', $user_id)
            ->setParameter('is_completed', $is_completed)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function JournalSelectByIdAndUserIdAndIsVisibleV1($journal_id, $user_id, $is_visible)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT j, jj, jjq, jq, js, jsq FROM EventHorizonRpgBundle:Journal j LEFT JOIN j.journal jj LEFT JOIN jj.quest jjq JOIN j.quest jq LEFT JOIN j.subquest js LEFT JOIN js.quest jsq WHERE j.id = :journal_id AND j.user = :user_id AND j.is_visible = :is_visible AND jq.is_blocked = :is_blocked')
            ->setParameter('journal_id', $journal_id)
            ->setParameter('user_id', $user_id)
            ->setParameter('is_visible', $is_visible)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function JournalSelectByQuestIdAndUserIdV1($quest_id, $user_id)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT j, q FROM EventHorizonRpgBundle:Journal j JOIN j.quest q WHERE j.quest = :quest_id AND j.user = :user_id AND q.is_blocked = :is_blocked')
            ->setParameter('quest_id', $quest_id)
            ->setParameter('user_id', $user_id)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function JournalSelectByUserIdAndIsCompletedV1($user_id, $is_completed)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT j, jj, jjq, jq, js, jsq FROM EventHorizonRpgBundle:Journal j LEFT JOIN j.journal jj LEFT JOIN jj.quest jjq JOIN j.quest jq LEFT JOIN j.subquest js LEFT JOIN js.quest jsq WHERE j.user = :user_id AND j.is_completed = :is_completed AND j.is_hidden = :is_hidden AND jq.is_blocked = :is_blocked ORDER BY j.created_at DESC')
            ->setParameter('user_id', $user_id)
            ->setParameter('is_completed', $is_completed)
            ->setParameter('is_hidden', 0)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function JournalSelectByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $is_visible)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT j, jj, jjq, jq, js, jsq FROM EventHorizonRpgBundle:Journal j LEFT JOIN j.journal jj LEFT JOIN jj.quest jjq JOIN j.quest jq LEFT JOIN j.subquest js LEFT JOIN js.quest jsq WHERE j.user = :user_id AND j.is_completed = :is_completed AND j.is_hidden = :is_hidden AND j.is_visible = :is_visible AND jq.is_blocked = :is_blocked ORDER BY j.created_at DESC')
            ->setParameter('user_id', $user_id)
            ->setParameter('is_completed', $is_completed)
            ->setParameter('is_visible', $is_visible)
            ->setParameter('is_hidden', 0)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function JournalSelectByUserIdAndTypeAndIsCompletedV1($user_id, $type, $is_completed)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT j, jj, jjq, jq, js, jsq FROM EventHorizonRpgBundle:Journal j LEFT JOIN j.journal jj LEFT JOIN jj.quest jjq JOIN j.quest jq LEFT JOIN j.subquest js LEFT JOIN js.quest jsq WHERE j.user = :user_id AND j.is_completed = :is_completed AND j.is_hidden = :is_hidden AND jq.type = :type AND jq.is_blocked = :is_blocked ORDER BY j.created_at DESC')
            ->setParameter('user_id', $user_id)
            ->setParameter('type', $type)
            ->setParameter('is_completed', $is_completed)
            ->setParameter('is_hidden', 0)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function JournalSelectByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $is_visible)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT j, jj, jjq, jq, js, jsq FROM EventHorizonRpgBundle:Journal j LEFT JOIN j.journal jj LEFT JOIN jj.quest jjq JOIN j.quest jq LEFT JOIN j.subquest js LEFT JOIN js.quest jsq WHERE j.user = :user_id AND j.is_completed = :is_completed AND j.is_hidden = :is_hidden AND j.is_visible = :is_visible AND jq.type = :type AND jq.is_blocked = :is_blocked ORDER BY j.created_at DESC')
            ->setParameter('user_id', $user_id)
            ->setParameter('type', $type)
            ->setParameter('is_completed', $is_completed)
            ->setParameter('is_visible', $is_visible)
            ->setParameter('is_hidden', 0)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function JournalResultCacheV1($query)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_journal_v1');

        return $query;
    }

    public function JournalResultCacheByIdAndUserIdV1($query, $journal_id, $user_id)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_journal_id_'.$journal_id.'_user_id_'.$user_id.'_v1');

        return $query;
    }

    public function JournalResultCacheByIdAndUserIdAndIsVisibleV1($query, $journal_id, $user_id, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_journal_id_'.$journal_id.'_user_id_'.$user_id.'_is_visible_'.$is_visible.'_v1');

        return $query;
    }

    public function JournalResultCacheByUserIdAndIsCompletedV1($query, $user_id, $is_completed)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_journal_user_id_'.$user_id.'_is_completed_'.$is_completed.'_v1');

        return $query;
    }

    public function JournalResultCacheByUserIdAndIsCompletedAndIsVisibleV1($query, $user_id, $is_completed, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_journal_user_id_'.$user_id.'_is_completed_'.$is_completed.'_is_visible_'.$is_visible.'_v1');

        return $query;
    }

    public function JournalResultCacheByUserIdAndTypeAndIsCompletedV1($query, $user_id, $type, $is_completed)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_journal_user_id_'.$user_id.'_type_'.$type.'_is_completed_'.$is_completed.'_v1');

        return $query;
    }

    public function JournalResultCacheByUserIdAndTypeAndIsCompletedAndIsVisibleV1($query, $user_id, $type, $is_completed, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_journal_user_id_'.$user_id.'_type_'.$type.'_is_completed_'.$is_completed.'_is_visible_'.$is_visible.'_v1');

        return $query;
    }

    public function deleteJournalResultCacheByIdAndUserIdV1($journal_id, $user_id)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_journal_id_'.$journal_id.'_user_id_'.$user_id.'_v1');
    }

    public function deleteJournalResultCacheByIdAndUserIdAndIsVisibleV1($journal_id, $user_id, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_journal_id_'.$journal_id.'_user_id_'.$user_id.'_is_visible_'.$is_visible.'_v1');
    }

    public function deleteJournalResultCacheByUserIdAndIsCompletedV1($user_id, $is_completed)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_journal_user_id_'.$user_id.'_is_completed_'.$is_completed.'_v1');
    }

    public function deleteJournalResultCacheByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_journal_user_id_'.$user_id.'_is_completed_'.$is_completed.'_is_visible_'.$is_visible.'_v1');
    }

    public function deleteJournalResultCacheByUserIdAndTypeAndIsCompletedV1($user_id, $type, $is_completed)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_journal_user_id_'.$user_id.'_type_'.$type.'_is_completed_'.$is_completed.'_v1');
    }

    public function deleteJournalResultCacheByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_journal_user_id_'.$user_id.'_type_'.$type.'_is_completed_'.$is_completed.'_is_visible_'.$is_visible.'_v1');
    }

    public function getJournalByIdAndUserIdV1($journal_id, $user_id)
    {
        $query = $this->JournalSelectByIdAndUserIdV1($journal_id, $user_id);
        $query = $this->JournalResultCacheByIdAndUserIdV1($query, $journal_id, $user_id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getJournalByIdAndUserIdV2($journal_id, $user_id)
    {
        $query = $this->JournalSelectByIdAndUserIdV1($journal_id, $user_id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getJournalByIdAndUserIdAndIsCompletedV1($journal_id, $user_id, $is_completed)
    {
        $query = $this->JournalSelectByIdAndUserIdAndIsCompletedV1($journal_id, $user_id, $is_completed);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getJournalByIdAndUserIdAndIsVisibleV1($journal_id, $user_id, $is_visible)
    {
        $query = $this->JournalSelectByIdAndUserIdAndIsVisibleV1($journal_id, $user_id, $is_visible);
        $query = $this->JournalResultCacheByIdAndUserIdAndIsVisibleV1($query, $journal_id, $user_id, $is_visible);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getJournalByQuestIdAndUserIdV1($quest_id, $user_id)
    {
        $query = $this->JournalSelectByQuestIdAndUserIdV1($quest_id, $user_id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getJournalsV1()
    {
        $query = $this->JournalSelectV1();
        $query = $this->JournalResultCacheV1($query);

        return $query->getResult();
    }

    public function getJournalsByUserIdAndIsCompletedV1($user_id, $is_completed)
    {
        $query = $this->JournalSelectByUserIdAndIsCompletedV1($user_id, $is_completed);
        $query = $this->JournalResultCacheByUserIdAndIsCompletedV1($query, $user_id, $is_completed);

        return $query->getResult();
    }

    public function getJournalsByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $is_visible)
    {
        $query = $this->JournalSelectByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $is_visible);
        $query = $this->JournalResultCacheByUserIdAndIsCompletedAndIsVisibleV1($query, $user_id, $is_completed, $is_visible);

        return $query->getResult();
    }

    public function getJournalsByUserIdAndTypeAndIsCompletedV1($user_id, $type, $is_completed)
    {
        $query = $this->JournalSelectByUserIdAndTypeAndIsCompletedV1($user_id, $type, $is_completed);
        $query = $this->JournalResultCacheByUserIdAndTypeAndIsCompletedV1($query, $user_id, $type, $is_completed);

        return $query->getResult();
    }

    public function getJournalsByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $is_visible)
    {
        $query = $this->JournalSelectByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $is_visible);
        $query = $this->JournalResultCacheByUserIdAndTypeAndIsCompletedAndIsVisibleV1($query, $user_id, $type, $is_completed, $is_visible);

        return $query->getResult();
    }

    public function completeQuest(\EventHorizon\RpgBundle\Entity\Journal $journal, \EventHorizon\RpgBundle\Entity\User $user)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $attribute_reward_points = EventHorizonRpgBundle::getAttributeRewardPoints();
            $experience_reward_points = EventHorizonRpgBundle::getExperienceRewardPoints();
            $promotion_level = EventHorizonRpgBundle::getPromotionLevel();
            $skill_reward_points = EventHorizonRpgBundle::getSkillRewardPoints();
            $reward_awarded = 0;
            $user_id = $user->getId();
            $attribute_reward = $journal->getQuest()->getAttributeReward();
            $skill_reward = $journal->getQuest()->getSkillReward();
            $is_completed = 1;
            $is_visible = 1;
            $journal->setIsCompleted($is_completed);
            $journal->setCompletedAt(new \DateTime());
            $this->getEntityManager()->persist($journal);
            $this->getEntityManager()->flush();

            $result_cache = $this->deleteJournalResultCacheByUserIdAndIsCompletedV1($user_id, $is_completed);
            $result_cache = $this->deleteJournalResultCacheByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $is_visible);
            foreach (array('dream', 'standard', 'training') as $type) {
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedV1($user_id, $type, $is_completed);
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $is_visible);
            }

            $is_completed = 0;
            $result_cache = $this->deleteJournalResultCacheByUserIdAndIsCompletedV1($user_id, $is_completed);
            $result_cache = $this->deleteJournalResultCacheByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $is_visible);
            foreach (array('dream', 'standard', 'training') as $type) {
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedV1($user_id, $type, $is_completed);
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $is_visible);
            }

            $character = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdV2($user_id);

            if ($attribute_reward == "agility") {
                $character->setAttributeAgility($character->getAttributeAgility() + $attribute_reward_points);
            } elseif ($attribute_reward == "charisma") {
                $character->setAttributeCharisma($character->getAttributeCharisma() + $attribute_reward_points);
            } elseif ($attribute_reward == "endurance") {
                $character->setAttributeEndurance($character->getAttributeEndurance() + $attribute_reward_points);
            } elseif ($attribute_reward == "intelligence") {
                $character->setAttributeIntelligence($character->getAttributeIntelligence() + $attribute_reward_points);
            } elseif ($attribute_reward == "luck") {
                $character->setAttributeLuck($character->getAttributeLuck() + $attribute_reward_points);
            } elseif ($attribute_reward == "perception") {
                $character->setAttributePerception($character->getAttributePerception() + $attribute_reward_points);
            } elseif ($attribute_reward == "strength") {
                $character->setAttributeStrength($character->getAttributeStrength() + $attribute_reward_points);
            } elseif ($attribute_reward == "willpower") {
                $character->setAttributeWillpower($character->getAttributeWillpower() + $attribute_reward_points);
            } elseif ($attribute_reward == "wisdom") {
                $character->setAttributeWisdom($character->getAttributeWisdom() + $attribute_reward_points);
            }

            if ($skill_reward == "art") {
                $character->setSkillArt($character->getSkillArt() + $skill_reward_points);
            } elseif ($skill_reward == "culture") {
                $character->setSkillCulture($character->getSkillCulture() + $skill_reward_points);
            } elseif ($skill_reward == "entertainment") {
                $character->setSkillEntertainment($character->getSkillEntertainment() + $skill_reward_points);
            } elseif ($skill_reward == "helpfulness") {
                $character->setSkillHelpfulness($character->getSkillHelpfulness() + $skill_reward_points);
            } elseif ($skill_reward == "home") {
                $character->setSkillHome($character->getSkillHome() + $skill_reward_points);
            } elseif ($skill_reward == "learning") {
                $character->setSkillLearning($character->getSkillLearning() + $skill_reward_points);
            } elseif ($skill_reward == "recreation") {
                $character->setSkillRecreation($character->getSkillRecreation() + $skill_reward_points);
            } elseif ($skill_reward == "sport") {
                $character->setSkillSport($character->getSkillSport() + $skill_reward_points);
            } elseif ($skill_reward == "work") {
                $character->setSkillWork($character->getSkillWork() + $skill_reward_points);
            }

            $character->setExperience($character->getExperience() + $experience_reward_points);

            $experience = $character->getExperience();
            $level = ($character->getLevel() + 1);

            if ((($level - 1) * $promotion_level) == $experience) {
                $reward = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Reward')->getRewardByUserIdAndLevelV1($user_id, $level);
                $reward_id = $reward->getId();
                $reward->setIsAwarded(1);
                $reward->setAwardedAt(new \DateTime());
                $this->getEntityManager()->persist($reward);
                $this->getEntityManager()->flush();
                $character->setLevel($character->getLevel() + 1);
                $reward_awarded = $reward_id;
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Reward')->deleteRewardResultCacheByUserIdV1($user_id);
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Reward')->deleteRewardResultCacheByUserIdAndIsVisibleV1($user_id, $is_visible);
            }

            $parent_journal = $journal->getJournal();
            if ($parent_journal) {
                $is_visible = 1;
                $parent_journal_id = $parent_journal->getId();
                $parent_journal = $this->getJournalByIdAndUserIdV2($parent_journal_id, $user_id);
                $parent_journal->setCompletedQuestCounter($parent_journal->getCompletedQuestCounter() + 1);
                $result_cache = $this->deleteJournalResultCacheByIdAndUserIdV1($parent_journal_id, $user_id);
                $result_cache = $this->deleteJournalResultCacheByIdAndUserIdAndIsVisibleV1($parent_journal_id, $user_id, $is_visible);
                $this->getEntityManager()->persist($parent_journal);
            }

            $this->getEntityManager()->persist($character);
            $this->getEntityManager()->flush();

            $this->getEntityManager()->getConnection()->commit();

            return $reward_awarded;
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }

    public function hideQuest(\EventHorizon\RpgBundle\Entity\Journal $journal, \EventHorizon\RpgBundle\Entity\User $user)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $is_completed = 0;
            $is_hidden = 1;
            $is_visible = 1;
            $user_id = $user->getId();
            $journal->setIsHidden($is_hidden);

            $parent_journal = $journal->getJournal();
            if ($parent_journal) {
                $parent_journal_id = $parent_journal->getId();
                $parent_journal = $this->getJournalByIdAndUserIdV2($parent_journal_id, $user_id);
                $parent_journal->setQuestCounter($parent_journal->getQuestCounter() - 1);
                $result_cache = $this->deleteJournalResultCacheByIdAndUserIdV1($parent_journal_id, $user_id);
                $result_cache = $this->deleteJournalResultCacheByIdAndUserIdAndIsVisibleV1($parent_journal_id, $user_id, $is_visible);
                $this->getEntityManager()->persist($parent_journal);
            }

            $this->getEntityManager()->persist($journal);
            $this->getEntityManager()->flush();

            $result_cache = $this->deleteJournalResultCacheByUserIdAndIsCompletedV1($user_id, $is_completed);
            $result_cache = $this->deleteJournalResultCacheByUserIdAndIsCompletedAndIsVisibleV1($user_id, $is_completed, $is_visible);
            foreach (array('dream', 'standard', 'training') as $type) {
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedV1($user_id, $type, $is_completed);
                $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Journal')->deleteJournalResultCacheByUserIdAndTypeAndIsCompletedAndIsVisibleV1($user_id, $type, $is_completed, $is_visible);
            }

            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }
}
