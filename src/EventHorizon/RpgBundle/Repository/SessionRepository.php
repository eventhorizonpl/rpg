<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SessionRepository extends EntityRepository
{
    public function SessionDeleteV1()
    {
        $query = $this->getEntityManager()
            ->createQuery('DELETE FROM EventHorizonRpgBundle:Session');

        return $query;
    }

    public function removeSessionV1()
    {
        $query = $this->SessionDeleteV1();

        return $query->execute();
    }
}
