<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;
use EventHorizon\RpgBundle\Cache;

class PageRepository extends EntityRepository
{
    public function PageSelectByLocaleV1($locale)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT p FROM EventHorizonRpgBundle:Page p WHERE p.locale = :locale AND p.is_visible = :is_visible ORDER BY p.title')
            ->setParameter('locale', $locale)
            ->setParameter('is_visible', 1);

        return $query;
    }

    public function PageSelectByLocaleAndNameV1($locale, $name)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT p FROM EventHorizonRpgBundle:Page p WHERE p.locale = :locale AND p.name = :name AND p.is_visible = :is_visible')
            ->setParameter('locale', $locale)
            ->setParameter('name', $name)
            ->setParameter('is_visible', 1);

        return $query;
    }

    public function PageResultCacheByLocaleV1($query, $locale)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_page_locale_'.$locale.'_v1');

        return $query;
    }

    public function PageResultCacheByLocaleAndNameV1($query, $locale, $name)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_page_locale_'.$locale.'_name_'.$name.'_v1');

        return $query;
    }

    public function getPageByLocaleAndNameV1($locale, $name)
    {
        $query = $this->PageSelectByLocaleAndNameV1($locale, $name);
        $query = $this->PageResultCacheByLocaleAndNameV1($query, $locale, $name);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getPagesByLocaleV1($locale)
    {
        $query = $this->PageSelectByLocaleV1($locale);
        $query = $this->PageResultCacheByLocaleV1($query, $locale);

        return $query->getResult();
    }
}
