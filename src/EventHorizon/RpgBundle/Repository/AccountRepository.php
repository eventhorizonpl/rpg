<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AccountRepository extends EntityRepository
{
    public function AccountSelectByUserIdV1($user_id)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT a FROM EventHorizonRpgBundle:Account a WHERE a.user = :user_id')
            ->setParameter('user_id', $user_id);

        return $query;
    }

    public function getAccountByUserIdV1($user_id)
    {
        $query = $this->AccountSelectByUserIdV1($user_id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function saveEditAccount(\EventHorizon\RpgBundle\Entity\Account $account, \EventHorizon\RpgBundle\Entity\User $user)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $user_id = $user->getId();
            $is_visible = 1;

            $this->getEntityManager()->persist($account);
            $this->getEntityManager()->flush();

            $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Character')->deleteCharacterResultCacheByUserIdV1($user_id);

            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }
}
