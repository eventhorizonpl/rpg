<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;
use EventHorizon\RpgBundle\Cache;

class SetupRepository extends EntityRepository
{
    protected static $setup = null;

    public function SetupSelectV1()
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT s FROM EventHorizonRpgBundle:Setup s');

        return $query;
    }

    public function SetupResultCacheV1($query)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_setup_v1');

        return $query;
    }

    public function deleteSetupResultCacheV1()
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_setup_v1');
    }

    public function getSetup()
    {
        if (is_null(self::$setup)) {
            self::$setup = $this->getSetupV1();
        }

        return self::$setup;
    }

    public function getSetupV1()
    {
        $query = $this->SetupSelectV1();
        $query = $this->SetupResultCacheV1($query);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getSetupV2()
    {
        $query = $this->SetupSelectV1();

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
