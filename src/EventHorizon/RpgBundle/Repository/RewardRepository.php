<?php

namespace EventHorizon\RpgBundle\Repository;

use Doctrine\ORM\EntityRepository;
use EventHorizon\RpgBundle\Cache;

class RewardRepository extends EntityRepository
{
    public function RewardSelectV1()
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT r FROM EventHorizonRpgBundle:Reward r');

        return $query;
    }

    public function RewardSelectByIdAndUserIdV1($reward_id, $user_id)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT r FROM EventHorizonRpgBundle:Reward r WHERE r.id = :id AND r.user = :user_id AND r.is_blocked = :is_blocked')
            ->setParameter('id', $reward_id)
            ->setParameter('user_id', $user_id)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function RewardSelectByIdAndUserIdAndIsAwardedV1($reward_id, $user_id, $is_awarded)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT r FROM EventHorizonRpgBundle:Reward r WHERE r.id = :id AND r.user = :user_id AND r.is_awarded = :is_awarded AND r.is_blocked = :is_blocked')
            ->setParameter('id', $reward_id)
            ->setParameter('user_id', $user_id)
            ->setParameter('is_awarded', $is_awarded)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function RewardSelectByIdAndUserIdAndIsAwardedAndIsReceivedV1($reward_id, $user_id, $is_awarded, $is_received)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT r FROM EventHorizonRpgBundle:Reward r WHERE r.id = :id AND r.user = :user_id AND r.is_awarded = :is_awarded AND r.is_blocked = :is_blocked AND r.is_received = :is_received')
            ->setParameter('id', $reward_id)
            ->setParameter('user_id', $user_id)
            ->setParameter('is_awarded', $is_awarded)
            ->setParameter('is_received', $is_received)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function RewardSelectByIdAndUserIdAndIsVisibleV1($reward_id, $user_id, $is_visible)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT r FROM EventHorizonRpgBundle:Reward r WHERE r.id = :id AND r.user = :user_id AND r.is_blocked = :is_blocked AND r.is_visible = :is_visible')
            ->setParameter('id', $reward_id)
            ->setParameter('user_id', $user_id)
            ->setParameter('is_blocked', 0)
            ->setParameter('is_visible', $is_visible);

        return $query;
    }

    public function RewardSelectByUserIdV1($user_id)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT r FROM EventHorizonRpgBundle:Reward r WHERE r.user = :user_id AND r.is_blocked = :is_blocked ORDER BY r.level DESC')
            ->setParameter('user_id', $user_id)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function RewardSelectByUserIdAndLevelV1($user_id, $level)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT r FROM EventHorizonRpgBundle:Reward r WHERE r.user = :user_id AND r.level = :level AND r.is_blocked = :is_blocked ORDER BY r.level DESC')
            ->setParameter('user_id', $user_id)
            ->setParameter('level', $level)
            ->setParameter('is_blocked', 0);

        return $query;
    }

    public function RewardSelectByUserIdAndIsVisibleV1($user_id, $is_visible)
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT r FROM EventHorizonRpgBundle:Reward r WHERE r.user = :user_id AND r.is_blocked = :is_blocked AND r.is_visible = :is_visible ORDER BY r.level DESC')
            ->setParameter('user_id', $user_id)
            ->setParameter('is_blocked', 0)
            ->setParameter('is_visible', $is_visible);

        return $query;
    }

    public function RewardResultCacheByIdAndUserIdV1($query, $reward_id, $user_id)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_reward_id_'.$reward_id.'_user_id_'.$user_id.'_v1');

        return $query;
    }

    public function RewardResultCacheByIdAndUserIdAndIsVisibleV1($query, $reward_id, $user_id, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_reward_id_'.$reward_id.'_user_id_'.$user_id.'_is_visible_'.$is_visible.'_v1');

        return $query;
    }

    public function RewardResultCacheByUserIdV1($query, $user_id)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_reward_user_id_'.$user_id.'_v1');

        return $query;
    }

    public function RewardResultCacheByUserIdAndIsVisibleV1($query, $user_id, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $query = $query->setResultCacheDriver($cacheDriver)
            ->useResultCache(true, Cache::getDefaultLifetime(), 'result_cache_reward_user_id_'.$user_id.'_is_visible_'.$is_visible.'_v1');

        return $query;
    }

    public function deleteRewardResultCacheByIdAndUserIdV1($reward_id, $user_id)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_reward_id_'.$reward_id.'_user_id_'.$user_id.'_v1');
    }

    public function deleteRewardResultCacheByIdAndUserIdAndIsVisibleV1($reward_id, $user_id, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_reward_id_'.$reward_id.'_user_id_'.$user_id.'_is_visible_'.$is_visible.'_v1');
    }

    public function deleteRewardResultCacheByUserIdV1($user_id)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_reward_user_id_'.$user_id.'_v1');
    }

    public function deleteRewardResultCacheByUserIdAndIsVisibleV1($user_id, $is_visible)
    {
        $cacheDriver = Cache::getCacheDriver();
        $cacheDriver->delete('result_cache_reward_user_id_'.$user_id.'_is_visible_'.$is_visible.'_v1');
    }

    public function getRewardByIdAndUserIdV1($reward_id, $user_id)
    {
        $query = $this->RewardSelectByIdAndUserIdV1($reward_id, $user_id);
        $query = $this->RewardResultCacheByIdAndUserIdV1($query, $reward_id, $user_id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getRewardByIdAndUserIdAndIsAwardedV1($reward_id, $user_id, $is_awarded)
    {
        $query = $this->RewardSelectByIdAndUserIdAndIsAwardedV1($reward_id, $user_id, $is_awarded);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getRewardByIdAndUserIdAndIsAwardedAndIsReceivedV1($reward_id, $user_id, $is_awarded, $is_received)
    {
        $query = $this->RewardSelectByIdAndUserIdAndIsAwardedAndIsReceivedV1($reward_id, $user_id, $is_awarded, $is_received);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getRewardByIdAndUserIdAndIsVisibleV1($reward_id, $user_id, $is_visible)
    {
        $query = $this->RewardSelectByIdAndUserIdAndIsVisibleV1($reward_id, $user_id, $is_visible);
        $query = $this->RewardResultCacheByIdAndUserIdAndIsVisibleV1($query, $reward_id, $user_id, $is_visible);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getRewardByUserIdAndLevelV1($user_id, $level)
    {
        $query = $this->RewardSelectByUserIdAndLevelV1($user_id, $level);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getRewardsV1()
    {
        $query = $this->RewardSelectV1();

        return $query->getResult();
    }

    public function getRewardsByUserIdV1($user_id)
    {
        $query = $this->RewardSelectByUserIdV1($user_id);
        $query = $this->RewardResultCacheByUserIdV1($query, $user_id);

        return $query->getResult();
    }

    public function getRewardsByUserIdAndLevelV1($user_id, $level)
    {
        $query = $this->RewardSelectByUserIdAndLevelV1($user_id, $level);

        return $query->getResult();
    }

    public function getRewardsByUserIdAndIsVisibleV1($user_id, $is_visible)
    {
        $query = $this->RewardSelectByUserIdAndIsVisibleV1($user_id, $is_visible);
        $query = $this->RewardResultCacheByUserIdAndIsVisibleV1($query, $user_id, $is_visible);

        return $query->getResult();
    }

    public function saveNewReward(\EventHorizon\RpgBundle\Entity\Reward $reward, \EventHorizon\RpgBundle\Entity\User $user)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $is_visible = 1;
            $user_id = $user->getId();
            $character = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Character')->getCharacterByUserIdV1($user_id);
            $level = ($character->getLevel() + 1);

            $reward->setCharacter($character);
            $reward->setUser($user);
            $reward->setLevel($level);
            $reward->setCreatedAt(new \DateTime());
            $reward->setUpdatedAt(new \DateTime());
            $this->getEntityManager()->persist($reward);
            $this->getEntityManager()->flush();

            $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Reward')->deleteRewardResultCacheByUserIdV1($user_id);
            $result_cache = $this->getEntityManager()->getRepository('EventHorizonRpgBundle:Reward')->deleteRewardResultCacheByUserIdAndIsVisibleV1($user_id, $is_visible);

            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }

    public function saveEditReward(\EventHorizon\RpgBundle\Entity\Reward $reward, \EventHorizon\RpgBundle\Entity\User $user)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $is_visible = 1;
            $reward_id = $reward->getId();
            $user_id = $user->getId();

            $this->getEntityManager()->persist($reward);
            $this->getEntityManager()->flush();

            $result_cache = $this->deleteRewardResultCacheByIdAndUserIdV1($reward_id, $user_id);
            $result_cache = $this->deleteRewardResultCacheByIdAndUserIdAndIsVisibleV1($reward_id, $user_id, $is_visible);
            $result_cache = $this->deleteRewardResultCacheByUserIdV1($user_id);
            $result_cache = $this->deleteRewardResultCacheByUserIdAndIsVisibleV1($user_id, $is_visible);

            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }

    public function receiveReward(\EventHorizon\RpgBundle\Entity\Reward $reward, \EventHorizon\RpgBundle\Entity\User $user)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $user_id = $user->getId();
            $is_received = 1;
            $reward->setIsReceived($is_received);
            $this->getEntityManager()->persist($reward);
            $this->getEntityManager()->flush();

            $result_cache = $this->deleteRewardResultCacheByUserIdV1($user_id);
            $result_cache = $this->deleteRewardResultCacheByUserIdAndIsVisibleV1($user_id, 1);

            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            $this->getEntityManager()->close();
            throw $e;
        }
    }
}
