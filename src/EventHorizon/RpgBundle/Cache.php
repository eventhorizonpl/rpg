<?php

namespace EventHorizon\RpgBundle;

class Cache
{
    public static function getCacheDriver()
    {
        $memcache = new \Memcache();
        $memcache->connect('localhost', 11211);
        $cacheDriver = new \Doctrine\Common\Cache\MemcacheCache();
        $cacheDriver->setMemcache($memcache);
        $cacheDriver->setNamespace('eh_rpg');

        return $cacheDriver;
    }

    public static function getDefaultLifetime()
    {
        return 3600;
    }
}
