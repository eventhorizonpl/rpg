<?php

namespace EventHorizon\RpgBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventHorizon\RpgBundle\Entity\Setup
 *
 * @ORM\Table(name="setup")
 * @ORM\Entity(repositoryClass="EventHorizon\RpgBundle\Repository\SetupRepository")
 */
class Setup
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $copyright_holder
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(length=255, name="copyright_holder", type="string")
     */
    private $copyright_holder;

    /**
     * @var string $copyright_holder_email
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(length=255, name="copyright_holder_email", type="string")
     */
    private $copyright_holder_email;

    /**
     * @var string $domain
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(length=255, name="domain", type="string")
     */
    private $domain;

    /**
     * @var string $game_contact_email
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(length=255, name="game_contact_email", type="string")
     */
    private $game_contact_email;

    /**
     * @var string $game_name
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(length=255, name="game_name", type="string")
     */
    private $game_name;

    /**
     * @var integer $statistics_enabled
     *
     * @ORM\Column(name="statistics_enabled", type="integer")
     */
    private $statistics_enabled = 0;

    /**
     * @var string $statistics_key
     *
     * @Assert\MaxLength(250)
     * @ORM\Column(length=255, name="statistics_key", nullable=true, type="string")
     */
    private $statistics_key;

    /**
     * @var integer $test_mode
     *
     * @ORM\Column(name="test_mode", type="integer")
     */
    private $test_mode = 0;

    /**
     * @var string $theme
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(length=255, name="theme", type="string")
     */
    private $theme;

    /**
     * @var string $url
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(length=255, name="url", type="string")
     */
    private $url;

    /**
     * @var datetime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get copyright_holder
     *
     * @return string
     */
    public function getCopyrightHolder()
    {
        return $this->copyright_holder;
    }

    /**
     * Set copyright_holder
     *
     * @param string $copyrightHolder
     */
    public function setCopyrightHolder($copyrightHolder)
    {
        $this->copyright_holder = $copyrightHolder;
    }

    /**
     * Get copyright_holder_email
     *
     * @return string
     */
    public function getCopyrightHolderEmail()
    {
        return $this->copyright_holder_email;
    }

    /**
     * Set copyright_holder_email
     *
     * @param string $copyrightHolderEmail
     */
    public function setCopyrightHolderEmail($copyrightHolderEmail)
    {
        $this->copyright_holder_email = $copyrightHolderEmail;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set domain
     *
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * Get game_contact_email
     *
     * @return string
     */
    public function getGameContactEmail()
    {
        return $this->game_contact_email;
    }

    /**
     * Set game_contact_email
     *
     * @param string $gameContactEmail
     */
    public function setGameContactEmail($gameContactEmail)
    {
        $this->game_contact_email = $gameContactEmail;
    }

    /**
     * Get game_name
     *
     * @return string
     */
    public function getGameName()
    {
        return $this->game_name;
    }

    /**
     * Set game_name
     *
     * @param string $gameName
     */
    public function setGameName($gameName)
    {
        $this->game_name = $gameName;
    }

    /**
     * Get statistics_enabled
     *
     * @return integer
     */
    public function getStatisticsEnabled()
    {
        return $this->statistics_enabled;
    }

    /**
     * Set statistics_enabled
     *
     * @param integer $statistics_enabled
     */
    public function setStatisticsEnabled($statistics_enabled)
    {
        $this->statistics_enabled = $statistics_enabled;
    }

    /**
     * Get statistics_key
     *
     * @return string
     */
    public function getStatisticsKey()
    {
        return $this->statistics_key;
    }

    /**
     * Set statistics_key
     *
     * @param string $statistics_key
     */
    public function setStatisticsKey($statistics_key)
    {
        $this->statistics_key = $statistics_key;
    }

    /**
     * Get test_mode
     *
     * @return integer
     */
    public function getTestMode()
    {
        return $this->test_mode;
    }

    /**
     * Set test_mode
     *
     * @param integer $test_mode
     */
    public function setTestMode($test_mode)
    {
        $this->test_mode = $test_mode;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set theme
     *
     * @param string $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }
}
