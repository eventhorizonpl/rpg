<?php

namespace EventHorizon\RpgBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventHorizon\RpgBundle\Entity\Reward
 *
 * @ORM\Table(
 *   name="reward",
 *   indexes={
 *     @ORM\Index(name="reward_main_01_index", columns={"id", "character_id", "user_id", "level", "is_awarded", "is_blocked", "is_visible", "awarded_at", "created_at", "updated_at"}),
 *     @ORM\Index(name="reward_01_index", columns={"is_received"})
 *   })
 * @ORM\Entity(repositoryClass="EventHorizon\RpgBundle\Repository\RewardRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Reward
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $character_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\Character")
     * @ORM\ManyToOne(cascade={"remove"}, inversedBy="reward", targetEntity="EventHorizon\RpgBundle\Entity\Character")
     * @ORM\JoinColumn(name="character_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $character;

    /**
     * @var integer $user_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\User")
     * @ORM\ManyToOne(cascade={"remove"}, inversedBy="reward", targetEntity="EventHorizon\RpgBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string $content
     *
     * @Assert\MaxLength(16384)
     * @Assert\NotBlank()
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var integer $level
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level = 0;

    /**
     * @var string $photo
     *
     * @ORM\Column(length=255, name="photo", nullable=true, type="string")
     */
    private $photo;

    /**
     * @Assert\File(
     *     maxSize="256k",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
     * )
     */
    private $photo_file;

    /**
     * @var string $title
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var integer $is_awarded
     *
     * @ORM\Column(name="is_awarded", type="integer")
     */
    private $is_awarded = 0;

    /**
     * @var integer $is_blocked
     *
     * @ORM\Column(name="is_blocked", type="integer")
     */
    private $is_blocked = 0;

    /**
     * @var integer $is_received
     *
     * @ORM\Column(name="is_received", type="integer")
     */
    private $is_received = 0;

    /**
     * @var integer $is_visible
     *
     * @ORM\Column(name="is_visible", type="integer")
     */
    private $is_visible = 0;

    /**
     * @var datetime $awarded_at
     *
     * @ORM\Column(name="awarded_at", nullable=true, type="datetime")
     */
    private $awarded_at;

    /**
     * @var datetime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get character_id
     *
     * @return integer
     */
    public function getCharacterId()
    {
        return $this->character_id;
    }

    /**
     * Set character
     *
     * @param object $character
     */
    public function setCharacter(Character $character)
    {
        $this->character = $character;
    }

    /**
     * Set character_id
     *
     * @param integer $character_id
     */
    public function setCharacterId($character_id)
    {
        $this->character_id = $character_id;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Set user_id
     *
     * @param integer $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set level
     *
     * @param string $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set photo
     *
     * @param string $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * Get photo_file
     */
    public function getPhotoFile()
    {
        return $this->photo_file;
    }

    /**
     * Set photo_file
     */
    public function setPhotoFile($photo_file)
    {
        $this->photo_file = $photo_file;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get is_awarded
     *
     * @return integer
     */
    public function getIsAwarded()
    {
        return $this->is_awarded;
    }

    /**
     * Set is_awarded
     *
     * @param integer $is_awarded
     */
    public function setIsAwarded($is_awarded)
    {
        $this->is_awarded = $is_awarded;
    }

    /**
     * Get is_blocked
     *
     * @return integer
     */
    public function getIsBlocked()
    {
        return $this->is_blocked;
    }

    /**
     * Set is_blocked
     *
     * @param integer $is_blocked
     */
    public function setIsBlocked($is_blocked)
    {
        $this->is_blocked = $is_blocked;
    }

    /**
     * Get is_received
     *
     * @return integer
     */
    public function getIsReceived()
    {
        return $this->is_received;
    }

    /**
     * Set is_received
     *
     * @param integer $is_received
     */
    public function setIsReceived($is_received)
    {
        $this->is_received = $is_received;
    }

    /**
     * Get is_visible
     *
     * @return integer
     */
    public function getIsVisible()
    {
        return $this->is_visible;
    }

    /**
     * Set is_visible
     *
     * @param integer $is_visible
     */
    public function setIsVisible($is_visible)
    {
        $this->is_visible = $is_visible;
    }

    /**
     * Get awarded_at
     *
     * @return datetime
     */
    public function getAwardedAt()
    {
        return $this->awarded_at;
    }

    /**
     * Set awarded_at
     *
     * @param datetime $awarded_at
     */
    public function setAwardedAt($awarded_at)
    {
        $this->awarded_at = $awarded_at;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    public function getAbsolutePath()
    {
        return null === $this->photo ? null : $this->getUploadRootDir().'/'.$this->photo;
    }

    public function checkAbsolutePath()
    {
        $path = $this->getAbsolutePath();

        if ($path) {
            if (is_readable($path)) {
                return true;
            }
        }

        return false;
    }

    protected function getUploadDir()
    {
        return 'uploads/rewards';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    public function getWebPath()
    {
        return null === $this->photo ? null : $this->getUploadDir().'/'.$this->photo;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->photo_file) {
            $this->photo = uniqid().'.'.$this->photo_file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->photo_file) {
            return;
        }

        $this->photo_file->move($this->getUploadRootDir(), $this->photo);

        unset($this->photo_file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($photo_file = $this->getAbsolutePath()) {
            unlink($photo_file);
        }
    }
}
