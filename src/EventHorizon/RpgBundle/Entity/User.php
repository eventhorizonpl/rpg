<?php

namespace EventHorizon\RpgBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EventHorizon\RpgBundle\Entity\User
 *
 * @ORM\Table(
 *   name="rpg_user",
 *   indexes={
 *     @ORM\Index(name="iser_main_01_index", columns={"id", "username", "username_canonical", "email", "email_canonical", "enabled", "last_login", "locked", "expired", "expires_at", "password_requested_at", "created_at", "updated_at"})},
 *   uniqueConstraints={@ORM\UniqueConstraint(columns={"email_canonical"}), @ORM\UniqueConstraint(columns={"username_canonical"})})
 * @ORM\Entity(repositoryClass="EventHorizon\RpgBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="EventHorizon\RpgBundle\Entity\Account", mappedBy="user")
     */
    protected $account;

    /**
     * @ORM\OneToOne(targetEntity="EventHorizon\RpgBundle\Entity\Character", mappedBy="user")
     */
    protected $character;

    /**
     * @ORM\OneToMany(targetEntity="EventHorizon\RpgBundle\Entity\Journal", mappedBy="user")
     */
    protected $journal;

    /**
     * @ORM\OneToMany(targetEntity="EventHorizon\RpgBundle\Entity\Quest", mappedBy="user")
     */
    protected $quest;

    /**
     * @ORM\OneToMany(targetEntity="EventHorizon\RpgBundle\Entity\Reward", mappedBy="user")
     */
    protected $reward;

    /**
     * @var string $facebook_id
     *
     * @ORM\Column(name="facebook_id", nullable=true, type="string")
     */
    protected $facebook_id;

    /**
     * @var datetime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    public function __construct()
    {
        parent::__construct();

        $this->journal = new ArrayCollection();
        $this->quest = new ArrayCollection();
        $this->reward = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Get character
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    public function serialize()
    {
        return serialize(array($this->facebook_id, parent::serialize()));
    }

    public function unserialize($data)
    {
        list($this->facebook_id, $parentData) = unserialize($data);
        parent::unserialize($parentData);
    }

    /**
     * @param string $facebook_id
     *
     * @return void
     */
    public function setFacebookId($facebook_id = null)
    {
        $this->facebook_id = $facebook_id;
        if ($this->username=="") {
            $this->setUsername($facebook_id);
        }
        $this->salt = '';
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * @param Array
     */
    public function setFBData($fbdata)
    {
        if (isset($fbdata['id'])) {
            $this->setFacebookId($fbdata['id']);
            //$this->addRole('ROLE_FACEBOOK');
            $this->setRoles(array('ROLE_PLAYER', 'ROLE_USER'));
        }
        if (isset($fbdata['email'])) {
            $this->setEmail($fbdata['email']);
        }
    }
}
