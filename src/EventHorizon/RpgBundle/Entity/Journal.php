<?php

namespace EventHorizon\RpgBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventHorizon\RpgBundle\Entity\Journal
 *
 * @ORM\Table(
 *   name="journal",
 *   indexes={
 *     @ORM\Index(name="journal_main_01_index", columns={"id", "character_id", "quest_id", "user_id", "is_completed", "is_hidden", "is_ordered_by_someone_else", "is_visible", "completed_at", "created_at", "updated_at"}),
 *     @ORM\Index(name="journal_01_index", columns={"publish_on_facebook"}),
 *     @ORM\Index(name="journal_02_index", columns={"journal_id", "completed_quest_counter", "quest_counter"})
 *   })
 * @ORM\Entity(repositoryClass="EventHorizon\RpgBundle\Repository\JournalRepository")
 */
class Journal
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $character_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\Character")
     * @ORM\ManyToOne(cascade={"remove"}, inversedBy="journal", targetEntity="EventHorizon\RpgBundle\Entity\Character")
     * @ORM\JoinColumn(name="character_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $character;

    /**
     * @var integer $journal_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\Journal")
     * @ORM\ManyToOne(cascade={"remove"}, inversedBy="subquest", targetEntity="EventHorizon\RpgBundle\Entity\Journal")
     * @ORM\JoinColumn(name="journal_id", nullable=true, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $journal;

    /**
     * @var integer $quest_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\Quest")
     * @ORM\ManyToOne(cascade={"remove"}, inversedBy="journal", targetEntity="EventHorizon\RpgBundle\Entity\Quest")
     * @ORM\JoinColumn(name="quest_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $quest;

    /**
     * @ORM\OneToMany(targetEntity="EventHorizon\RpgBundle\Entity\Journal", mappedBy="journal")
     */
    protected $subquest;

    /**
     * @var integer $user_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\User")
     * @ORM\ManyToOne(cascade={"remove"}, inversedBy="journal", targetEntity="EventHorizon\RpgBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var integer $is_completed
     *
     * @ORM\Column(name="is_completed", type="integer")
     */
    private $is_completed = 0;

    /**
     * @var integer $is_hidden
     *
     * @ORM\Column(name="is_hidden", type="integer")
     */
    private $is_hidden = 0;

    /**
     * @var integer $is_ordered_by_someone_else
     *
     * @ORM\Column(name="is_ordered_by_someone_else", type="integer")
     */
    private $is_ordered_by_someone_else = 0;

    /**
     * @var integer $is_visible
     *
     * @ORM\Column(name="is_visible", type="integer")
     */
    private $is_visible = 0;

    /**
     * @var integer $publish_on_facebook
     *
     * @ORM\Column(name="publish_on_facebook", type="integer")
     */
    private $publish_on_facebook = 0;

    /**
     * @var integer $completed_quest_counter
     *
     * @ORM\Column(name="completed_quest_counter", type="integer")
     */
    private $completed_quest_counter = 0;

    /**
     * @var integer $quest_counter
     *
     * @ORM\Column(name="quest_counter", type="integer")
     */
    private $quest_counter = 0;

    /**
     * @var datetime $completed_at
     *
     * @ORM\Column(name="completed_at", nullable=true, type="datetime")
     */
    private $completed_at;

    /**
     * @var datetime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    public function __construct()
    {
        $this->subquest = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get character_id
     *
     * @return integer
     */
    public function getCharacterId()
    {
        return $this->character_id;
    }

    /**
     * Set character
     *
     * @param object $character
     */
    public function setCharacter(Character $character)
    {
        $this->character = $character;
    }

    /**
     * Set character_id
     *
     * @param integer $character_id
     */
    public function setCharacterId($character_id)
    {
        $this->character_id = $character_id;
    }

    /**
     * Get journal
     *
     * @return object
     */
    public function getJournal()
    {
        return $this->journal;
    }

    /**
     * Set journal
     *
     * @param object $journal
     */
    public function setJournal(Journal $journal)
    {
        $this->journal = $journal;
    }

    /**
     * Get quest
     *
     * @return object
     */
    public function getQuest()
    {
        return $this->quest;
    }

    /**
     * Get quest_id
     *
     * @return integer
     */
    public function getQuestId()
    {
        return $this->quest_id;
    }

    /**
     * Set quest
     *
     * @param object $quest
     */
    public function setQuest(Quest $quest)
    {
        $this->quest = $quest;
    }

    /**
     * Set quest_id
     *
     * @param integer $quest_id
     */
    public function setQuestId($quest_id)
    {
        $this->quest_id = $quest_id;
    }

    /**
     * Get subquest
     *
     * @return object
     */
    public function getSubquest()
    {
        return $this->subquest;
    }

    /**
     * Set subquest
     *
     * @param object $subquest
     */
    public function setSubquest(Journal $subquest)
    {
        $this->subquest = $subquest;
    }

    /**
     * Get user
     *
     * @return object
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Set user_id
     *
     * @param integer $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Get is_completed
     *
     * @return integer
     */
    public function getIsCompleted()
    {
        return $this->is_completed;
    }

    /**
     * Set is_completed
     *
     * @param integer $is_completed
     */
    public function setIsCompleted($is_completed)
    {
        $this->is_completed = $is_completed;
    }

    /**
     * Get is_hidden
     *
     * @return integer
     */
    public function getIsHidden()
    {
        return $this->is_hidden;
    }

    /**
     * Set is_hidden
     *
     * @param integer $is_hidden
     */
    public function setIsHidden($is_hidden)
    {
        $this->is_hidden = $is_hidden;
    }

    /**
     * Get is_ordered_by_someone_else
     *
     * @return integer
     */
    public function getIsOrderedBySomeoneElse()
    {
        return $this->is_ordered_by_someone_else;
    }

    /**
     * Set is_ordered_by_someone_else
     *
     * @param integer $is_ordered_by_someone_else
     */
    public function setIsOrderedBySomeoneElse($is_ordered_by_someone_else)
    {
        $this->is_ordered_by_someone_else = $is_ordered_by_someone_else;
    }

    /**
     * Get is_visible
     *
     * @return integer
     */
    public function getIsVisible()
    {
        return $this->is_visible;
    }

    /**
     * Set is_visible
     *
     * @param integer $is_visible
     */
    public function setIsVisible($is_visible)
    {
        $this->is_visible = $is_visible;
    }

    /**
     * Get publish_on_facebook
     *
     * @return integer
     */
    public function getPublishOnFacebook()
    {
        return $this->publish_on_facebook;
    }

    /**
     * Set publish_on_facebook
     *
     * @param integer $publish_on_facebook
     */
    public function setPublishOnFacebook($publish_on_facebook)
    {
        $this->publish_on_facebook = $publish_on_facebook;
    }

    /**
     * Get completed_quest_counter
     *
     * @return integer
     */
    public function getCompletedQuestCounter()
    {
        return $this->completed_quest_counter;
    }

    /**
     * Set completed_quest_counter
     *
     * @param integer $completed_quest_counter
     */
    public function setCompletedQuestCounter($completed_quest_counter)
    {
        $this->completed_quest_counter = $completed_quest_counter;
    }

    /**
     * Get quest_counter
     *
     * @return integer
     */
    public function getQuestCounter()
    {
        return $this->quest_counter;
    }

    /**
     * Set quest_counter
     *
     * @param integer $quest_counter
     */
    public function setQuestCounter($quest_counter)
    {
        $this->quest_counter = $quest_counter;
    }

    /**
     * Get completed_at
     *
     * @return datetime
     */
    public function getCompletedAt()
    {
        return $this->completed_at;
    }

    /**
     * Set completed_at
     *
     * @param datetime $completedAt
     */
    public function setCompletedAt($completedAt)
    {
        $this->completed_at = $completedAt;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }
}
