<?php

namespace EventHorizon\RpgBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventHorizon\RpgBundle\Entity\UserGroup
 *
 * @ORM\Table(
 *   name="rpg_user_group",
 *   indexes={
 *     @ORM\Index(name="user_group_main_01_index", columns={"id", "group_id", "user_id", "created_at", "updated_at"})})
 * @ORM\Entity(repositoryClass="EventHorizon\RpgBundle\Repository\UserGroupRepository")
 */
class UserGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $group_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\Group")
     * @ORM\ManyToOne(cascade={"remove"}, targetEntity="EventHorizon\RpgBundle\Entity\Group")
     * @ORM\JoinColumn(name="group_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $group;

    /**
     * @var integer $user_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\User")
     * @ORM\ManyToOne(cascade={"remove"}, targetEntity="EventHorizon\RpgBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var datetime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get group_id
     *
     * @return integer
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * Set group
     *
     * @param object $group
     */
    public function setGroup(Group $group)
    {
        $this->group = $group;
    }

    /**
     * Set group_id
     *
     * @param integer $group_id
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Set user_id
     *
     * @param integer $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }
}
