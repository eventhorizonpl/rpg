<?php

namespace EventHorizon\RpgBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventHorizon\RpgBundle\Entity\Account
 *
 * @ORM\Table(
 *   name="account",
 *   indexes={
 *     @ORM\Index(name="account_main_01_index", columns={"id", "user_id", "type", "expires_at", "created_at", "updated_at"}),
 *     @ORM\Index(name="account_01_index", columns={"default_journal_is_visible", "default_journal_publish_on_facebook", "default_reward_is_visible"})
 *   },
 *   uniqueConstraints={@ORM\UniqueConstraint(columns={"user_id"})})
 * @ORM\Entity(repositoryClass="EventHorizon\RpgBundle\Repository\AccountRepository")
 */
class Account
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $user_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\User")
     * @ORM\OneToOne(cascade={"remove"}, inversedBy="account", targetEntity="EventHorizon\RpgBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var integer $default_journal_is_visible
     *
     * @ORM\Column(name="default_journal_is_visible", type="integer")
     */
    private $default_journal_is_visible = 0;

    /**
     * @var integer $default_journal_publish_on_facebook
     *
     * @ORM\Column(name="default_journal_publish_on_facebook", type="integer")
     */
    private $default_journal_publish_on_facebook = 0;

    /**
     * @var integer $default_reward_is_visible
     *
     * @ORM\Column(name="default_reward_is_visible", type="integer")
     */
    private $default_reward_is_visible = 0;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var datetime $expires_at
     *
     * @ORM\Column(name="expires_at", nullable=false, type="datetime")
     */
    private $expires_at;

    /**
     * @var datetime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Set user_id
     *
     * @param integer $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Get default_journal_is_visible
     *
     * @return integer
     */
    public function getDefaultJournalIsVisible()
    {
        return $this->default_journal_is_visible;
    }

    /**
     * Set default_journal_is_visible
     *
     * @param integer $default_journal_is_visible
     */
    public function setDefaultJournalIsVisible($default_journal_is_visible)
    {
        $this->default_journal_is_visible = $default_journal_is_visible;
    }

    /**
     * Get default_journal_publish_on_facebook
     *
     * @return integer
     */
    public function getDefaultJournalPublishOnFacebook()
    {
        return $this->default_journal_publish_on_facebook;
    }

    /**
     * Set default_journal_publish_on_facebook
     *
     * @param integer $default_journal_publish_on_facebook
     */
    public function setDefaultJournalPublishOnFacebook($default_journal_publish_on_facebook)
    {
        $this->default_journal_publish_on_facebook = $default_journal_publish_on_facebook;
    }

    /**
     * Get default_reward_is_visible
     *
     * @return integer
     */
    public function getDefaultRewardIsVisible()
    {
        return $this->default_reward_is_visible;
    }

    /**
     * Set default_reward_is_visible
     *
     * @param integer $default_reward_is_visible
     */
    public function setDefaultRewardIsVisible($default_reward_is_visible)
    {
        $this->default_reward_is_visible = $default_reward_is_visible;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get expires_at
     *
     * @return datetime
     */
    public function getExpiresAt()
    {
        return $this->expires_at;
    }

    /**
     * Set expires_at
     *
     * @param datetime $expiresAt
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expires_at = $expiresAt;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }
}
