<?php

namespace EventHorizon\RpgBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventHorizon\RpgBundle\Entity\Session
 *
 * @ORM\Table(name="session")
 * @ORM\Entity(repositoryClass="EventHorizon\RpgBundle\Repository\SessionRepository")
 */
class Session
{
    /**
     * @var string $session_id
     *
     * @ORM\Column(name="session_id", type="string", length=255)
     * @ORM\Id
     */
    private $session_id;

    /**
     * @var text $session_value
     *
     * @ORM\Column(name="session_value", type="text")
     */
    private $session_value;

    /**
     * @var integer $session_time
     *
     * @ORM\Column(name="session_time", type="integer")
     */
    private $session_time;
}
