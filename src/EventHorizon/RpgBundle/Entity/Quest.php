<?php

namespace EventHorizon\RpgBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventHorizon\RpgBundle\Entity\Quest
 *
 * @ORM\Table(
 *   name="quest",
 *   indexes={
 *     @ORM\Index(name="quest_main_01_index", columns={"id", "character_id", "user_id", "attribute_reward", "skill_reward", "is_blocked", "is_group_quest", "created_at", "updated_at"}),
 *     @ORM\Index(name="quest_01_index", columns={"type"})
 *   })
 * @ORM\Entity(repositoryClass="EventHorizon\RpgBundle\Repository\QuestRepository")
 */
class Quest
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $character_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\Character")
     * @ORM\ManyToOne(cascade={"remove"}, inversedBy="quest", targetEntity="EventHorizon\RpgBundle\Entity\Character")
     * @ORM\JoinColumn(name="character_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $character;

    /**
     * @ORM\OneToMany(targetEntity="EventHorizon\RpgBundle\Entity\Journal", mappedBy="quest")
     */
    protected $journal;

    /**
     * @var integer $user_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\User")
     * @ORM\ManyToOne(cascade={"remove"}, inversedBy="quest", targetEntity="EventHorizon\RpgBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string $attribute_reward
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="attribute_reward", type="string", length=255)
     */
    private $attribute_reward;

    /**
     * @var string $content
     *
     * @Assert\MaxLength(16384)
     * @Assert\NotBlank()
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string $skill_reward
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="skill_reward", type="string", length=255)
     */
    private $skill_reward;

    /**
     * @var string $title
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string $type
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var integer $is_blocked
     *
     * @ORM\Column(name="is_blocked", type="integer")
     */
    private $is_blocked = 0;

    /**
     * @var integer $is_group_quest
     *
     * @ORM\Column(name="is_group_quest", type="integer")
     */
    private $is_group_quest = 0;

    /**
     * @var datetime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    public function __construct()
    {
        $this->journal = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get character_id
     *
     * @return integer
     */
    public function getCharacterId()
    {
        return $this->character_id;
    }

    /**
     * Set character
     *
     * @param object $character
     */
    public function setCharacter(Character $character)
    {
        $this->character = $character;
    }

    /**
     * Set character_id
     *
     * @param integer $character_id
     */
    public function setCharacterId($character_id)
    {
        $this->character_id = $character_id;
    }

    /**
     * Get journal
     *
     * @return integer
     */
    public function getJournal()
    {
        return $this->journal;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Set user_id
     *
     * @param integer $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Get attribute_reward
     *
     * @return string
     */
    public function getAttributeReward()
    {
        return $this->attribute_reward;
    }

    /**
     * Set attribute_reward
     *
     * @param string $attributeReward
     */
    public function setAttributeReward($attributeReward)
    {
        $this->attribute_reward = $attributeReward;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get skill_reward
     *
     * @return string
     */
    public function getSkillReward()
    {
        return $this->skill_reward;
    }

    /**
     * Set skill_reward
     *
     * @param string $skillReward
     */
    public function setSkillReward($skillReward)
    {
        $this->skill_reward = $skillReward;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get is_blocked
     *
     * @return integer
     */
    public function getIsBlocked()
    {
        return $this->is_blocked;
    }

    /**
     * Set is_blocked
     *
     * @param integer $is_blocked
     */
    public function setIsBlocked($is_blocked)
    {
        $this->is_blocked = $is_blocked;
    }

    /**
     * Get is_group_quest
     *
     * @return integer
     */
    public function getIsGroupQuest()
    {
        return $this->is_group_quest;
    }

    /**
     * Set is_group_quest
     *
     * @param integer $is_group_quest
     */
    public function setIsGroupQuest($is_group_quest)
    {
        $this->is_group_quest = $is_group_quest;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    public function __toString()
    {
        return $this->id;
    }
}
