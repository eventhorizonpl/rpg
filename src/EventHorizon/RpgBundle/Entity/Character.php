<?php

namespace EventHorizon\RpgBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventHorizon\RpgBundle\Entity\Character
 *
 * @ORM\Table(
 *   name="character",
 *   indexes={
 *     @ORM\Index(name="character_main_01_index", columns={"id", "user_id", "birth_date", "experience", "gender", "level", "name", "is_visible", "created_at", "updated_at"}),
 *     @ORM\Index(name="character_main_02_index", columns={"attribute_agility", "attribute_charisma", "attribute_endurance", "attribute_intelligence", "attribute_luck", "attribute_perception", "attribute_strength", "attribute_willpower", "attribute_wisdom"}),
 *     @ORM\Index(name="character_main_03_index", columns={"skill_art", "skill_culture", "skill_entertainment", "skill_helpfulness", "skill_home", "skill_learning", "skill_recreation", "skill_sport", "skill_work"})},
 *   uniqueConstraints={@ORM\UniqueConstraint(columns={"user_id"})})
 * @ORM\Entity(repositoryClass="EventHorizon\RpgBundle\Repository\CharacterRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Character
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="EventHorizon\RpgBundle\Entity\Journal", mappedBy="character")
     */
    protected $journal;

    /**
     * @ORM\OneToMany(targetEntity="EventHorizon\RpgBundle\Entity\Quest", mappedBy="character")
     */
    protected $quest;

    /**
     * @ORM\OneToMany(targetEntity="EventHorizon\RpgBundle\Entity\Reward", mappedBy="character")
     */
    protected $reward;

    /**
     * @var integer $user_id
     *
     * @Assert\Type(type="EventHorizon\RpgBundle\Entity\User")
     * @ORM\OneToOne(cascade={"remove"}, inversedBy="character", targetEntity="EventHorizon\RpgBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var float $attribute_agility
     *
     * @ORM\Column(name="attribute_agility", type="float")
     */
    private $attribute_agility = 0;

    /**
     * @var float $attribute_charisma
     *
     * @ORM\Column(name="attribute_charisma", type="float")
     */
    private $attribute_charisma = 0;

    /**
     * @var float $attribute_endurance
     *
     * @ORM\Column(name="attribute_endurance", type="float")
     */
    private $attribute_endurance = 0;

    /**
     * @var float $attribute_intelligence
     *
     * @ORM\Column(name="attribute_intelligence", type="float")
     */
    private $attribute_intelligence = 0;

    /**
     * @var float $attribute_luck
     *
     * @ORM\Column(name="attribute_luck", type="float")
     */
    private $attribute_luck = 0;

    /**
     * @var float $attribute_perception
     *
     * @ORM\Column(name="attribute_perception", type="float")
     */
    private $attribute_perception = 0;

    /**
     * @var float $attribute_strength
     *
     * @ORM\Column(name="attribute_strength", type="float")
     */
    private $attribute_strength = 0;

    /**
     * @var float $attribute_willpower
     *
     * @ORM\Column(name="attribute_willpower", type="float")
     */
    private $attribute_willpower = 0;

    /**
     * @var float $attribute_wisdom
     *
     * @ORM\Column(name="attribute_wisdom", type="float")
     */
    private $attribute_wisdom = 0;

    /**
     * @var string $avatar
     *
     * @ORM\Column(length=255, name="avatar", nullable=true, type="string")
     */
    private $avatar;

    /**
     * @Assert\File(
     *     maxSize="256k",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
     * )
     */
    private $avatar_file;

    /**
     * @var \DateTime $birth_date
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="birth_date", nullable=true, type="date")
     */
    private $birth_date;

    /**
     * @var integer $experience
     *
     * @ORM\Column(name="experience", type="integer")
     */
    private $experience = 0;

    /**
     * @var string $gender
     *
     * @Assert\Choice(
     *     choices = { "male", "female" },
     *     message = "Choose a valid gender"
     * )
     * @Assert\NotBlank()
     * @ORM\Column(length=255, name="gender", nullable=true, type="string")
     */
    private $gender;

    /**
     * @var integer $level
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level = 0;

    /**
     * @var string $name
     *
     * @Assert\MaxLength(250)
     * @Assert\NotBlank()
     * @ORM\Column(length=255, name="name", nullable=true, type="string")
     */
    private $name;

    /**
     * @var float $skill_art
     *
     * @ORM\Column(name="skill_art", type="float")
     */
    private $skill_art = 0;

    /**
     * @var float $skill_culture
     *
     * @ORM\Column(name="skill_culture", type="float")
     */
    private $skill_culture = 0;

    /**
     * @var float $skill_entertainment
     *
     * @ORM\Column(name="skill_entertainment", type="float")
     */
    private $skill_entertainment = 0;

    /**
     * @var float $skill_helpfulness
     *
     * @ORM\Column(name="skill_helpfulness", type="float")
     */
    private $skill_helpfulness = 0;

    /**
     * @var float $skill_home
     *
     * @ORM\Column(name="skill_home", type="float")
     */
    private $skill_home = 0;

    /**
     * @var float $skill_learning
     *
     * @ORM\Column(name="skill_learning", type="float")
     */
    private $skill_learning = 0;

    /**
     * @var float $skill_recreation
     *
     * @ORM\Column(name="skill_recreation", type="float")
     */
    private $skill_recreation = 0;

    /**
     * @var float $skill_sport
     *
     * @ORM\Column(name="skill_sport", type="float")
     */
    private $skill_sport = 0;

    /**
     * @var float $skill_work
     *
     * @ORM\Column(name="skill_work", type="float")
     */
    private $skill_work = 0;

    /**
     * @var integer $is_visible
     *
     * @ORM\Column(name="is_visible", type="integer")
     */
    private $is_visible = 0;

    /**
     * @var datetime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var datetime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated_at;

    public function __construct()
    {
        $this->journal = new ArrayCollection();
        $this->quest = new ArrayCollection();
        $this->reward = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user
     *
     * @param object $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Set user_id
     *
     * @param integer $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Get attribute_agility
     *
     * @return float
     */
    public function getAttributeAgility()
    {
        return $this->attribute_agility;
    }

    /**
     * Set attribute_agility
     *
     * @param float $attribute_agility
     */
    public function setAttributeAgility($attribute_agility)
    {
        $this->attribute_agility = $attribute_agility;
    }

    /**
     * Get attribute_charisma
     *
     * @return float
     */
    public function getAttributeCharisma()
    {
        return $this->attribute_charisma;
    }

    /**
     * Set attribute_charisma
     *
     * @param float $attribute_charisma
     */
    public function setAttributeCharisma($attribute_charisma)
    {
        $this->attribute_charisma = $attribute_charisma;
    }

    /**
     * Get attribute_endurance
     *
     * @return float
     */
    public function getAttributeEndurance()
    {
        return $this->attribute_endurance;
    }

    /**
     * Set attribute_endurance
     *
     * @param float $attribute_endurance
     */
    public function setAttributeEndurance($attribute_endurance)
    {
        $this->attribute_endurance = $attribute_endurance;
    }

    /**
     * Get attribute_intelligence
     *
     * @return float
     */
    public function getAttributeIntelligence()
    {
        return $this->attribute_intelligence;
    }

    /**
     * Set attribute_intelligence
     *
     * @param float $attribute_intelligence
     */
    public function setAttributeIntelligence($attribute_intelligence)
    {
        $this->attribute_intelligence = $attribute_intelligence;
    }

    /**
     * Get attribute_luck
     *
     * @return float
     */
    public function getAttributeLuck()
    {
        return $this->attribute_luck;
    }

    /**
     * Set attribute_luck
     *
     * @param float $attribute_luck
     */
    public function setAttributeLuck($attribute_luck)
    {
        $this->attribute_luck = $attribute_luck;
    }

    /**
     * Get attribute_perception
     *
     * @return float
     */
    public function getAttributePerception()
    {
        return $this->attribute_perception;
    }

    /**
     * Set attribute_perception
     *
     * @param float $attribute_perception
     */
    public function setAttributePerception($attribute_perception)
    {
        $this->attribute_perception = $attribute_perception;
    }

    /**
     * Get attribute_strength
     *
     * @return float
     */
    public function getAttributeStrength()
    {
        return $this->attribute_strength;
    }

    /**
     * Set attribute_strength
     *
     * @param float $attribute_strength
     */
    public function setAttributeStrength($attribute_strength)
    {
        $this->attribute_strength = $attribute_strength;
    }

    /**
     * Get attribute_willpower
     *
     * @return float
     */
    public function getAttributeWillpower()
    {
        return $this->attribute_willpower;
    }

    /**
     * Set attribute_willpower
     *
     * @param float $attribute_willpower
     */
    public function setAttributeWillpower($attribute_willpower)
    {
        $this->attribute_willpower = $attribute_willpower;
    }

    /**
     * Get attribute_wisdom
     *
     * @return float
     */
    public function getAttributeWisdom()
    {
        return $this->attribute_wisdom;
    }

    /**
     * Set attribute_wisdom
     *
     * @param float $attribute_wisdom
     */
    public function setAttributeWisdom($attribute_wisdom)
    {
        $this->attribute_wisdom = $attribute_wisdom;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * Get avatar_file
     */
    public function getAvatarFile()
    {
        return $this->avatar_file;
    }

    /**
     * Set avatar
     */
    public function setAvatarFile($avatar_file)
    {
        $this->avatar_file = $avatar_file;
    }

    /**
     * Get birth_date
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * Set birth_date
     *
     * @param \DateTime $birth_date
     */
    public function setBirthDate($birthDate)
    {
        $this->birth_date = $birthDate;
    }

    /**
     * Get experience
     *
     * @return integer
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set experience
     *
     * @param integer $experience
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set gender
     *
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set level
     *
     * @param integer $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get skill_art
     *
     * @return float
     */
    public function getSkillArt()
    {
        return $this->skill_art;
    }

    /**
     * Set skill_art
     *
     * @param float $skill_art
     */
    public function setSkillArt($skill_art)
    {
        $this->skill_art = $skill_art;
    }

    /**
     * Get skill_culture
     *
     * @return float
     */
    public function getSkillCulture()
    {
        return $this->skill_culture;
    }

    /**
     * Set skill_culture
     *
     * @param float $skill_culture
     */
    public function setSkillCulture($skill_culture)
    {
        $this->skill_culture = $skill_culture;
    }

    /**
     * Get skill_entertainment
     *
     * @return float
     */
    public function getSkillEntertainment()
    {
        return $this->skill_entertainment;
    }

    /**
     * Set skill_entertainment
     *
     * @param float $skill_entertainment
     */
    public function setSkillEntertainment($skill_entertainment)
    {
        $this->skill_entertainment = $skill_entertainment;
    }

    /**
     * Get skill_helpfulness
     *
     * @return float
     */
    public function getSkillHelpfulness()
    {
        return $this->skill_helpfulness;
    }

    /**
     * Set skill_helpfulness
     *
     * @param float $skill_helpfulness
     */
    public function setSkillHelpfulness($skill_helpfulness)
    {
        $this->skill_helpfulness = $skill_helpfulness;
    }

    /**
     * Get skill_home
     *
     * @return float
     */
    public function getSkillHome()
    {
        return $this->skill_home;
    }

    /**
     * Set skill_home
     *
     * @param float $skill_home
     */
    public function setSkillHome($skill_home)
    {
        $this->skill_home = $skill_home;
    }

    /**
     * Get skill_learning
     *
     * @return float
     */
    public function getSkillLearning()
    {
        return $this->skill_learning;
    }

    /**
     * Set skill_learning
     *
     * @param float $skill_learning
     */
    public function setSkillLearning($skill_learning)
    {
        $this->skill_learning = $skill_learning;
    }

    /**
     * Get skill_recreation
     *
     * @return float
     */
    public function getSkillRecreation()
    {
        return $this->skill_recreation;
    }

    /**
     * Set skill_recreation
     *
     * @param float $skill_recreation
     */
    public function setSkillRecreation($skill_recreation)
    {
        $this->skill_recreation = $skill_recreation;
    }

    /**
     * Get skill_sport
     *
     * @return float
     */
    public function getSkillSport()
    {
        return $this->skill_sport;
    }

    /**
     * Set skill_sport
     *
     * @param float $skill_sport
     */
    public function setSkillSport($skill_sport)
    {
        $this->skill_sport = $skill_sport;
    }

    /**
     * Get skill_work
     *
     * @return float
     */
    public function getSkillWork()
    {
        return $this->skill_work;
    }

    /**
     * Set skill_work
     *
     * @param float $skill_work
     */
    public function setSkillWork($skill_work)
    {
        $this->skill_work = $skill_work;
    }

    /**
     * Get is_visible
     *
     * @return integer
     */
    public function getIsVisible()
    {
        return $this->is_visible;
    }

    /**
     * Set is_visible
     *
     * @param integer $is_visible
     */
    public function setIsVisible($is_visible)
    {
        $this->is_visible = $is_visible;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    public function __toString()
    {
        return $this->id;
    }

    public function getAbsolutePath()
    {
        return null === $this->avatar ? null : $this->getUploadRootDir().'/'.$this->avatar;
    }

    public function checkAbsolutePath()
    {
        $path = $this->getAbsolutePath();

        if ($path) {
            if (is_readable($path)) {
                return true;
            }
        }

        return false;
    }

    protected function getUploadDir()
    {
        return 'uploads/avatars';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    public function getWebPath()
    {
        return null === $this->avatar ? null : $this->getUploadDir().'/'.$this->avatar;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->avatar_file) {
            $this->avatar = uniqid().'.'.$this->avatar_file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->avatar_file) {
            return;
        }

        $this->avatar_file->move($this->getUploadRootDir(), $this->avatar);

        unset($this->avatar_file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($avatar_file = $this->getAbsolutePath()) {
            unlink($avatar_file);
        }
    }
}
