<?php
namespace EventHorizon\RpgBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class QuestTypeType extends AbstractType
{
    private $options = array();

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        if ($this->options['locale'] == "pl") {
            $resolver->setDefaults(array(
                'choices' => array(
                    'standard' => 'Standard',
                    'dream' => 'Dream',
                    'training' => 'Training',
                )
            ));
        } else {
            $resolver->setDefaults(array(
                'choices' => array(
                    'standard' => 'Standard',
                    'dream' => 'Dream',
                    'training' => 'Training',
                )
            ));
        }
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'quest_type';
    }
}
