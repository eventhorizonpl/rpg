<?php
namespace EventHorizon\RpgBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AttributeRewardType extends AbstractType
{
    private $options = array();

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        if ($this->options['locale'] == "pl") {
            $resolver->setDefaults(array(
                'choices' => array(
                    'charisma' => 'Charisma',
                    'intelligence' => 'Intelligence',
                    'wisdom' => 'Wisdom',
                    'perception' => 'Perception',
                    'strength' => 'Strength',
                    'willpower' => 'Willpower',
                    'luck' => 'Luck',
                    'endurance' => 'Endurance',
                    'agility' => 'Agility',
                )
            ));
        } else {
            $resolver->setDefaults(array(
                'choices' => array(
                    'agility' => 'Agility',
                    'charisma' => 'Charisma',
                    'endurance' => 'Endurance',
                    'intelligence' => 'Intelligence',
                    'luck' => 'Luck',
                    'perception' => 'Perception',
                    'strength' => 'Strength',
                    'willpower' => 'Willpower',
                    'wisdom' => 'Wisdom',
                )
            ));
        }
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'attribute_reward';
    }
}
