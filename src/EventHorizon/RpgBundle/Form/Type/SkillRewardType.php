<?php
namespace EventHorizon\RpgBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SkillRewardType extends AbstractType
{
    private $options = array();

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        if ($this->options['locale'] == "pl") {
            $resolver->setDefaults(array(
                'choices' => array(
                    'home' => 'Home',
                    'culture' => 'Culture',
                    'learning' => 'Learning',
                    'work' => 'Work',
                    'recreation' => 'Recreation',
                    'entertainment' => 'Entertainment',
                    'sport' => 'Sport',
                    'art' => 'Art',
                    'helpfulness' => 'Helpfulness',
                )
            ));
        } else {
            $resolver->setDefaults(array(
                'choices' => array(
                    'art' => 'Art',
                    'culture' => 'Culture',
                    'entertainment' => 'Entertainment',
                    'helpfulness' => 'Helpfulness',
                    'home' => 'Home',
                    'learning' => 'Learning',
                    'recreation' => 'Recreation',
                    'sport' => 'Sport',
                    'work' => 'Work',
                )
            ));
        }
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'skill_reward';
    }
}
