<?php

namespace EventHorizon\RpgBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\NotBlank;
use EventHorizon\RpgBundle\Form\Type\BooleanType;
use EventHorizon\RpgBundle\Form\Type\GenderType;

class CharacterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('avatar_file')
            ->add('birth_date', 'date', array(
                'constraints' => array(new NotBlank()),
                'widget' => 'single_text'
            ))
            ->add('name', 'text', array(
                'constraints' => array(
                    new MaxLength(250),
                    new NotBlank(),
                )
            ))
            ->add('gender', new GenderType(), array('constraints' => array(new NotBlank())))
            ->add('is_visible', new BooleanType())
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventHorizon\RpgBundle\Entity\Character'
        ));
    }

    public function getName()
    {
        return 'eventhorizon_rpgbundle_charactertype';
    }
}
