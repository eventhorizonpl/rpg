<?php

namespace EventHorizon\RpgBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\NotBlank;
use EventHorizon\RpgBundle\Form\Type\BooleanType;

class RewardType extends AbstractType
{
    private $options = array();

    public function __construct(array $options = array())
    {
        $this->options = $options;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', 'textarea', array(
                'constraints' => array(
                    new MaxLength(16384),
                    new NotBlank(),
                )
            ))
            ->add('photo_file')
            ->add('title', 'text', array(
                'constraints' => array(
                    new MaxLength(250),
                    new NotBlank(),
                )
            ))
        ;

        if (isset($this->options['user'])) {
            $builder->add('is_visible', new BooleanType(), array(
                    'data' => $this->options['user']->getAccount()->getDefaultRewardIsVisible(),
                ))
            ;
        } else {
            $builder->add('is_visible', new BooleanType());
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventHorizon\RpgBundle\Entity\Reward'
        ));
    }

    public function getName()
    {
        return 'eventhorizon_rpgbundle_rewardtype';
    }
}
