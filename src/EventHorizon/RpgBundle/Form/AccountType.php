<?php

namespace EventHorizon\RpgBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use EventHorizon\RpgBundle\Form\Type\BooleanType;

class AccountType extends AbstractType
{
    private $options = array();

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('default_journal_is_visible', new BooleanType())
            ->add('default_reward_is_visible', new BooleanType())
        ;
        if ($this->options['user']->getFacebookId()) {
            $builder
                ->add('default_journal_publish_on_facebook', new BooleanType())
            ;
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventHorizon\RpgBundle\Entity\Account'
        ));
    }

    public function getName()
    {
        return 'eventhorizon_rpgbundle_accounttype';
    }
}
