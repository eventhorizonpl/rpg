<?php

namespace EventHorizon\RpgBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Validator\Constraints\NotBlank;
use EventHorizon\RpgBundle\Form\Type\AttributeRewardType;
use EventHorizon\RpgBundle\Form\Type\BooleanType;
use EventHorizon\RpgBundle\Form\Type\QuestTypeType;
use EventHorizon\RpgBundle\Form\Type\SkillRewardType;

class QuestType extends AbstractType
{
    private $options = array();

    public function __construct(array $options)
    {
        $this->options = $options;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('attribute_reward', new AttributeRewardType(array('locale' => $this->options['locale'])), array(
                'constraints' => array(new NotBlank())
            ))
            ->add('content', 'textarea', array(
                'constraints' => array(
                    new MaxLength(16384),
                    new NotBlank(),
                )
            ))
            ->add('skill_reward', new SkillRewardType(array('locale' => $this->options['locale'])), array(
                'constraints' => array(new NotBlank())
            ))
            ->add('title', 'text', array(
                'constraints' => array(
                    new MaxLength(250),
                    new NotBlank(),
                )
            ))
            ->add('type', new QuestTypeType(array('locale' => $this->options['locale'])), array(
                'constraints' => array(new NotBlank())
            ))
        ;

        if (isset($this->options['journal'])) {
            $builder->add('is_visible', new BooleanType(), array(
                    'data' => $this->options['journal']->getIsVisible(),
                    'property_path' => false,
                ))
            ;
        } elseif (isset($this->options['user'])) {
            $builder->add('is_visible', new BooleanType(), array(
                    'data' => $this->options['user']->getAccount()->getDefaultJournalIsVisible(),
                    'property_path' => false,
                ))
            ;
        } else {
            $builder->add('is_visible', new BooleanType(), array(
                    'property_path' => false,
                ))
            ;
        }

        if (isset($this->options['journal'])) {
            $builder->add('publish_on_facebook', new BooleanType(), array(
                    'data' => $this->options['journal']->getPublishOnFacebook(),
                    'property_path' => false,
                ))
            ;
        } elseif (isset($this->options['user'])) {
            $builder->add('publish_on_facebook', new BooleanType(), array(
                    'data' => $this->options['user']->getAccount()->getDefaultJournalPublishOnFacebook(),
                    'property_path' => false,
                ))
            ;
        } else {
            $builder->add('publish_on_facebook', new BooleanType(), array(
                    'property_path' => false,
                ))
            ;
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventHorizon\RpgBundle\Entity\Quest'
        ));
    }

    public function getName()
    {
        return 'eventhorizon_rpgbundle_questtype';
    }
}
