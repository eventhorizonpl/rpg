<?php

namespace EventHorizon\RpgBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VacuumDbCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rpg:vacuum-db')
            ->setDescription('Vacuum database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager();

        $this->em->getConnection()->exec('VACUUM;');
    }
}
