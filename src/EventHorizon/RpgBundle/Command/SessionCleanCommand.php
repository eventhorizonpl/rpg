<?php

namespace EventHorizon\RpgBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SessionCleanCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rpg:session-clean')
            ->setDescription('Session clean')
            ->addOption('run', null, InputOption::VALUE_NONE, 'If set, the task will clean sessions')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager();

        if ($input->getOption('run')) {
            $this->sessions = $this->em->getRepository('EventHorizonRpgBundle:Session')->removeSessionV1();
        }
    }
}
