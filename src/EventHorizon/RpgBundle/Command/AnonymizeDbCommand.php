<?php

namespace EventHorizon\RpgBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AnonymizeDbCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rpg:anonymize-db')
            ->setDescription('Anonymize database')
            ->addOption('run', null, InputOption::VALUE_NONE, 'If set, the task will anonymize database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager();
        $this->users = $this->em->getRepository('EventHorizonRpgBundle:User')->getUsersV1();

        foreach ($this->users as $this->user) {
            if ($input->getOption('run')) {
                $this->user->setPlainPassword('rpg');
                $this->user->setEmail('anonymize_'.$this->user->getEmail());
                $this->em->persist($this->user);
                $this->em->flush();
            }
        }

        $text = 'User count='.count($this->users);

        $output->writeln($text);
    }
}
