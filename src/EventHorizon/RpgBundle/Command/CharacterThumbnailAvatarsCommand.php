<?php

namespace EventHorizon\RpgBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CharacterThumbnailAvatarsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rpg:character-thumbnail-avatars')
            ->setDescription('Thumbnail avatars')
            ->addOption('run', null, InputOption::VALUE_NONE, 'If set, the task will thumbnail avatars')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->avatars_dir = 'web/uploads/avatars/';

        $this->all_avatars = shell_exec('ls '.$this->avatars_dir);
        $this->all_avatars = explode("\n", $this->all_avatars);

        foreach ($this->all_avatars as $this->avatar) {
            if (($input->getOption('run')) and ($this->avatar)) {
                $img = new \Imagick($this->avatars_dir.$this->avatar);
                if ($img->valid()) {
                    $img->thumbnailImage(200, 200, true);
                    $img->writeImage($this->avatars_dir.$this->avatar);
                }
            }
        }
    }
}
