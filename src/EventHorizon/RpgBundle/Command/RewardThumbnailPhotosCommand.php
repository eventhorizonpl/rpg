<?php

namespace EventHorizon\RpgBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RewardThumbnailPhotosCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rpg:reward-thumbnail-photos')
            ->setDescription('Thumbnail photos')
            ->addOption('run', null, InputOption::VALUE_NONE, 'If set, the task will thumbnail photos')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->rewards_dir = 'web/uploads/rewards/';

        $this->all_rewards = shell_exec('ls '.$this->rewards_dir);
        $this->all_rewards = explode("\n", $this->all_rewards);

        foreach ($this->all_rewards as $this->reward) {
            if (($input->getOption('run')) and ($this->reward)) {
                $img = new \Imagick($this->rewards_dir.$this->reward);
                if ($img->valid()) {
                    $img->thumbnailImage(200, 200, true);
                    $img->writeImage($this->rewards_dir.$this->reward);
                }
            }
        }
    }
}
