<?php

namespace EventHorizon\RpgBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CharacterFindObsoleteAvatarsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rpg:character-find-obsolete-avatars')
            ->setDescription('Find obsolete avatars')
            ->addOption('delete', null, InputOption::VALUE_NONE, 'If set, the task will delete obsolete avatars')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager();
        $this->characters = $this->em->getRepository('EventHorizonRpgBundle:Character')->getCharactersV1();

        $this->avatars_dir = 'web/uploads/avatars/';
        $this->avatars_cache_dir = 'web/media/cache/avatar_thumb/uploads/avatars/';
        $this->used_avatars = array();

        $this->all_avatars = shell_exec('ls '.$this->avatars_dir);
        $this->all_avatars = explode("\n", $this->all_avatars);

        foreach ($this->characters as $this->character) {
            if ($this->character->getAvatar()) {
                $this->used_avatars[] = $this->character->getAvatar();
            }
        }

        $this->obsolete_avatars = 0;
        $this->deleted_avatars = 0;
        $this->deleted_avatars_cache = 0;

        foreach ($this->all_avatars as $this->avatar) {
            if (($this->avatar) and (!(in_array($this->avatar, $this->used_avatars)))) {
                if ($input->getOption('delete')) {
                    if (is_writable($this->avatars_dir.$this->avatar)) {
                        unlink($this->avatars_dir.$this->avatar);
                        $this->deleted_avatars += 1;
                    }
                    if (is_writable($this->avatars_cache_dir.$this->avatar)) {
                        unlink($this->avatars_cache_dir.$this->avatar);
                        $this->deleted_avatars_cache += 1;
                    }
                }
                $this->obsolete_avatars += 1;
            }
        }

        $text = 'obsolete_avatars = '.$this->obsolete_avatars.' deleted_avatars = '.$this->deleted_avatars.' deleted_avatars_cache = '.$this->deleted_avatars_cache;

        $output->writeln($text);
    }
}
