<?php

namespace EventHorizon\RpgBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RewardFindObsoletePhotosCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rpg:reward-find-obsolete-photos')
            ->setDescription('Find obsolete photos')
            ->addOption('delete', null, InputOption::VALUE_NONE, 'If set, the task will delete obsolete photos')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager();
        $this->rewards = $this->em->getRepository('EventHorizonRpgBundle:Reward')->getRewardsV1();

        $this->rewards_dir = 'web/uploads/rewards/';
        $this->rewards_cache_dir = 'web/media/cache/reward_thumb/uploads/rewards/';
        $this->used_rewards = array();

        $this->all_rewards = shell_exec('ls '.$this->rewards_dir);
        $this->all_rewards = explode("\n", $this->all_rewards);

        foreach ($this->rewards as $this->reward) {
            if ($this->reward->getPhoto()) {
                $this->used_rewards[] = $this->reward->getPhoto();
            }
        }

        $this->obsolete_rewards = 0;
        $this->deleted_rewards = 0;
        $this->deleted_rewards_cache = 0;

        foreach ($this->all_rewards as $this->reward) {
            if (($this->reward) and (!(in_array($this->reward, $this->used_rewards)))) {
                if ($input->getOption('delete')) {
                    if (is_writable($this->rewards_dir.$this->reward)) {
                        unlink($this->rewards_dir.$this->reward);
                        $this->deleted_rewards += 1;
                    }
                    if (is_writable($this->rewards_cache_dir.$this->reward)) {
                        unlink($this->rewards_cache_dir.$this->reward);
                        $this->deleted_rewards_cache += 1;
                    }
                }
                $this->obsolete_rewards += 1;
            }
        }

        $text = 'obsolete_rewards = '.$this->obsolete_rewards.' deleted_rewards = '.$this->deleted_rewards.' deleted_rewards_cache = '.$this->deleted_rewards_cache;

        $output->writeln($text);
    }
}
