<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('reward', new Route(
    '/',
    array('_controller' => 'EventHorizonRpgBundle:Reward:index'),
    array('_method' => 'get')
));

$collection->add('reward_new', new Route(
    '/new',
    array('_controller' => 'EventHorizonRpgBundle:Reward:new'),
    array('_method' => 'get')
));

$collection->add('reward_create', new Route(
    '/new',
    array('_controller' => 'EventHorizonRpgBundle:Reward:create'),
    array('_method' => 'post')
));

$collection->add('reward_edit', new Route(
    '/{id}/edit',
    array('_controller' => 'EventHorizonRpgBundle:Reward:edit'),
    array('_method' => 'get')
));

$collection->add('reward_update', new Route(
    '/{id}/edit',
    array('_controller' => 'EventHorizonRpgBundle:Reward:update'),
    array('_method' => 'post')
));

$collection->add('reward_receive', new Route(
    '/{id}/receive',
    array('_controller' => 'EventHorizonRpgBundle:Reward:receive'),
    array('_method' => 'get')
));

$collection->add('reward_show', new Route(
    '/{id}',
    array('_controller' => 'EventHorizonRpgBundle:Reward:show'),
    array('_method' => 'get')
));

$collection->add('reward_show_awarded', new Route(
    '/{id}/show_awarded',
    array('_controller' => 'EventHorizonRpgBundle:Reward:showAwarded'),
    array('_method' => 'get')
));

return $collection;
