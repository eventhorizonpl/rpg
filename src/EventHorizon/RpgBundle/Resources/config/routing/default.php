<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('_homepage', new Route('/', array(
    '_controller' => 'EventHorizonRpgBundle:Default:index',
)));

$collection->add('language', new Route('/language/{language}', array(
    '_controller' => 'EventHorizonRpgBundle:Default:language',
)));

$collection->add('layout', new Route('/rpg/layout', array(
    '_controller' => 'EventHorizonRpgBundle:Default:layout',
)));

return $collection;
