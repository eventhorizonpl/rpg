<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('quest_new', new Route(
    '/new/{journal_id}',
    array(
        '_controller' => 'EventHorizonRpgBundle:Quest:new',
        'journal_id' => null
    ),
    array('_method' => 'get')
));

$collection->add('quest_create', new Route(
    '/new/{journal_id}',
    array(
        '_controller' => 'EventHorizonRpgBundle:Quest:create',
        'journal_id' => null
    ),
    array('_method' => 'post')
));

$collection->add('quest_edit', new Route(
    '/{id}/edit',
    array('_controller' => 'EventHorizonRpgBundle:Quest:edit'),
    array('_method' => 'get')
));

$collection->add('quest_update', new Route(
    '/{id}/edit',
    array('_controller' => 'EventHorizonRpgBundle:Quest:update'),
    array('_method' => 'post')
));

return $collection;
