<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('page', new Route(
    '/',
    array('_controller' => 'EventHorizonRpgBundle:Page:index'),
    array('_method' => 'get')
));

$collection->add('page_show', new Route(
    '/{name}',
    array('_controller' => 'EventHorizonRpgBundle:Page:show'),
    array('_method' => 'get')
));

return $collection;
