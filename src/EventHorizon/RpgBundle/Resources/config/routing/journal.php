<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('journal_complete', new Route(
    '/{id}/complete',
    array('_controller' => 'EventHorizonRpgBundle:Journal:complete'),
    array('_method' => 'get')
));

$collection->add('journal_hide', new Route(
    '/{id}/hide',
    array('_controller' => 'EventHorizonRpgBundle:Journal:hide'),
    array('_method' => 'get')
));

$collection->add('journal_show', new Route(
    '/{id}',
    array('_controller' => 'EventHorizonRpgBundle:Journal:show'),
    array(
        '_method' => 'get',
        'id'      => '\d+',
    )
));

$collection->add('journal', new Route(
    '/{mode}/{type}',
    array(
        '_controller' => 'EventHorizonRpgBundle:Journal:index',
        'mode' => 'todo',
        'type' => 'all'
    ),
    array('_method' => 'get')
));

return $collection;
