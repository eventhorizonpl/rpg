<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('character_show', new Route(
    '/',
    array('_controller' => 'EventHorizonRpgBundle:Character:show'),
    array('_method' => 'get')
));

$collection->add('character_edit', new Route(
    '/edit',
    array('_controller' => 'EventHorizonRpgBundle:Character:edit'),
    array('_method' => 'get')
));

$collection->add('character_update', new Route(
    '/edit',
    array('_controller' => 'EventHorizonRpgBundle:Character:update'),
    array('_method' => 'post')
));

return $collection;
