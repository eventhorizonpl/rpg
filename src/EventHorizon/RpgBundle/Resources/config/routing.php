<?php

use Symfony\Component\Routing\RouteCollection;

$collection = new RouteCollection();

$collection->addCollection($loader->import("@EventHorizonRpgBundle/Resources/config/routing/character.php"), '/character');
$collection->addCollection($loader->import("@EventHorizonRpgBundle/Resources/config/routing/default.php"), '/');
$collection->addCollection($loader->import("@EventHorizonRpgBundle/Resources/config/routing/journal.php"), '/journal');
$collection->addCollection($loader->import("@EventHorizonRpgBundle/Resources/config/routing/page.php"), '/page');
$collection->addCollection($loader->import("@EventHorizonRpgBundle/Resources/config/routing/quest.php"), '/quest');
$collection->addCollection($loader->import("@EventHorizonRpgBundle/Resources/config/routing/reward.php"), '/reward');
$collection->addCollection($loader->import("@EventHorizonRpgBundle/Resources/config/routing.yml"), '/');

return $collection;
