$(document).ready(function() {
    $("#eventhorizon_rpgbundle_charactertype_name").focus();
    $("#eventhorizon_rpgbundle_charactertype_birth_date").datepicker({ format: "yyyy-mm-dd" });
    $("#eventhorizon_rpgbundle_charactertype_public_profile_link").hide();
    $('#eventhorizon_rpgbundle_charactertype_is_visible').change(function() {
        $(this).find("option:selected").each(function() {
            if ($(this).val() == "1") {
                $("#eventhorizon_rpgbundle_charactertype_public_profile_link").show();
            } else {
                $("#eventhorizon_rpgbundle_charactertype_public_profile_link").hide();
            }
        });
    });
    if ($('#eventhorizon_rpgbundle_charactertype_is_visible option:selected').val() == "1") {
        $("#eventhorizon_rpgbundle_charactertype_public_profile_link").show();
    }
    $("#eventhorizon_rpgbundle_questtype_publish_on_facebook_select").hide();
    $('#eventhorizon_rpgbundle_questtype_is_visible').change(function() {
        $(this).find("option:selected").each(function() {
            if ($(this).val() == "1") {
                $("#eventhorizon_rpgbundle_questtype_publish_on_facebook_select").show();
            } else {
                $("#eventhorizon_rpgbundle_questtype_publish_on_facebook_select").hide();
            }
        });
    });
    if ($('#eventhorizon_rpgbundle_questtype_is_visible option:selected').val() == "1") {
        $("#eventhorizon_rpgbundle_questtype_publish_on_facebook_select").show();
    }
    $("#eventhorizon_rpgbundle_questtype_title").focus();
    $("#eventhorizon_rpgbundle_rewardtype_title").focus();
    $('[rel="popover"]').popover({
        html: true,
    });
    $('[rel="tooltip"]').tooltip();
    $('.carousel').carousel({
        interval: 5000
    });
    $(".complete_quest").confirmModal();
    $(".hide_quest").confirmModal();
});
