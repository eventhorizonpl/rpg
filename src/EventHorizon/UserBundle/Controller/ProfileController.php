<?php

namespace EventHorizon\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Controller\ProfileController as BaseController;

class ProfileController extends BaseController
{
    public function common()
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->setup = $this->em->getRepository('EventHorizonRpgBundle:Setup')->getSetup();
        $this->request = $this->container->get('request');
        $this->request->setLocale($this->container->get('session')->get('_locale'));
        $this->request->getSession()->set('referrer', $this->request->getRequestUri());
    }

    public function editAction()
    {
        $this->common();

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->container->get('fos_user.profile.form');
        $formHandler = $this->container->get('fos_user.profile.form.handler');

        $process = $formHandler->process($user);
        if ($process) {
            $this->setFlash('fos_user_success', 'profile.flash.updated');

            return new RedirectResponse($this->getRedirectionUrl($user));
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Profile:edit.html.'.$this->container->getParameter('fos_user.template.engine'), array(
            'form'  => $form->createView(),
            'setup' => $this->setup,
        ));
    }

    public function showAction()
    {
        $this->common();

        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Profile:show.html.'.$this->container->getParameter('fos_user.template.engine'), array(
            'setup' => $this->setup,
            'user'  => $user,
        ));
    }
}
