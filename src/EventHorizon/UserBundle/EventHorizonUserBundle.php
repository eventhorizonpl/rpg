<?php

namespace EventHorizon\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EventHorizonUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
