<?php

namespace EventHorizon\LogBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

/**
 * @MongoDB\Document(repositoryClass="EventHorizon\LogBundle\Repository\LogRepository")
 * @HasLifecycleCallbacks
 */
class Log
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\String
     */
    protected $content;

    /**
     * @MongoDB\String
     */
    protected $level;

    /**
     * @MongoDB\Timestamp
     */
    private $created_at;

    /**
     * Get id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get level
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set level
     *
     * @param string $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * Get created_at
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_at
     *
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @MongoDB\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \MongoTimestamp());
    }
}
