<?php

namespace EventHorizon\LogBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use EventHorizon\LogBundle\Document\Log;

class LogRepository extends DocumentRepository
{
    public function addLog($content, $level)
    {
        $log = new Log();
        $log->setContent($content);
        $log->setLevel($level);

        $this->getDocumentManager()->persist($log);
        $this->getDocumentManager()->flush();

        return $log;
    }
}
