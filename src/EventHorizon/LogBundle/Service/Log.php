<?php

namespace EventHorizon\LogBundle\Service;

class Log
{
    private $dm;

    private $level_debug = 100;
    private $level_info = 200;
    private $level_notice = 250;
    private $level_warning = 300;
    private $level_error = 400;
    private $level_critical = 500;
    private $level_alert = 550;
    private $level_emergency = 600;

    public function __construct($doctrine_mongodb_service)
    {
        $this->dm = $doctrine_mongodb_service->getManager();
    }

    public function add($content, $level)
    {
        $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $level);
    }

    public function addDebug($content)
    {
        $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_debug);
    }

    public function addInfo($content)
    {
        $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_info);
    }

    public function addNotice($content)
    {
        $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_notice);
    }

    public function addWarning($content)
    {
        $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_warning);
    }

    public function addError($content)
    {
        $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_error);
    }

    public function addCritical($content)
    {
        $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_critical);
    }

    public function addAlert($content)
    {
        $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_alert);
    }

    public function addEmergency($content)
    {
        $this->dm->getRepository('EventHorizonLogBundle:Log')->addLog($content, $this->level_emergency);
    }
}
