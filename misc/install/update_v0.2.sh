#!/bin/sh

echo "Tworzenie bazy mongo"
php app/console doctrine:mongodb:schema:create

echo "Migracja do wersji v0.2 (20121005000000)"
php app/console doctrine:migrations:migrate --no-interaction 20121005000000
