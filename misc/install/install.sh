#!/bin/sh

echo "Usuwanie bazy"
php app/console doctrine:database:drop --force

echo "Usuwanie bazy mongo"
php app/console doctrine:mongodb:schema:drop

echo "Tworzenie bazy"
php app/console doctrine:database:create

echo "Migracja do wersji v0.1 (20120727000000)"
php app/console doctrine:migrations:migrate --no-interaction 20120727000000

sh misc/install/update_v0.2.sh
sh misc/install/update_v0.3.sh
sh misc/install/update_v0.4.sh
