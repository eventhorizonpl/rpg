#!/bin/sh

if [ -f web/index.php ]
then
    rm web/index.php
fi

ln -s index.html web/index.php

#php app/console cache:clear
sudo rm -rf app/cache/dev/
sudo rm -rf app/cache/prod/
