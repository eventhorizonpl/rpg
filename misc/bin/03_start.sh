#!/bin/sh

sudo /etc/init.d/postgresql-9.1 restart
sudo /etc/init.d/mongod restart
sudo /etc/init.d/memcached restart
sudo /etc/init.d/httpd restart
sleep 30

sh misc/bin/project_enable.sh

sudo /etc/init.d/httpd restart
sudo /etc/init.d/crond start
