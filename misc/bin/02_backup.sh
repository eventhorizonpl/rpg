#!/bin/sh

DATE=`date +%y-%m-%d-%H-%M`
BACKUP_DIR=backup-$DATE
ROOT_DIR="/home/michal/"

if [ -d "$1" ]
then
    ROOT_DIR=$1
fi

mkdir $ROOT_DIR/backup/$BACKUP_DIR

UPLOADS_BACKUP_TAR=$ROOT_DIR/backup/$BACKUP_DIR/uploads-$DATE.tar
MEDIA_BACKUP_TAR=$ROOT_DIR/backup/$BACKUP_DIR/media-$DATE.tar
DB_BACKUP=$ROOT_DIR/backup/$BACKUP_DIR/rpg-$DATE.sql
MONGO_BACKUP=$ROOT_DIR/backup/$BACKUP_DIR/mongo-$DATE
MONGO_BACKUP_TAR=$MONGO_BACKUP.tar

tar -cvf $UPLOADS_BACKUP_TAR /data/rpg/web/uploads/
tar -cvf $MEDIA_BACKUP_TAR /data/rpg/web/media/
pg_dump rpg -U rpg > $DB_BACKUP
mongodump --db rpg -o $MONGO_BACKUP
tar -cvf $MONGO_BACKUP_TAR $MONGO_BACKUP
rm -rf $MONGO_BACKUP

bzip2 -9 $UPLOADS_BACKUP_TAR
bzip2 -9 $MEDIA_BACKUP_TAR
bzip2 -9 $DB_BACKUP
bzip2 -9 $MONGO_BACKUP_TAR
