#!/bin/sh

if [ -f web/index.php ]
then
    rm web/index.php
fi

ln -s app.php web/index.php

#php app/console cache:clear
sudo rm -rf app/cache/dev/
sudo rm -rf app/cache/prod/
php app/console cache:warmup --env=prod
