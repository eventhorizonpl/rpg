#!/bin/sh

for DIR in `find . -type d -print`
do
    echo -e "\ndir $DIR"
    chmod 775 $DIR
done

for FILE in `find . -type f -print`
do
    echo -e "\nfile $FILE"
    chmod 664 $FILE
done

chmod 775 app/console
