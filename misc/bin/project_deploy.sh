#!/bin/sh

if [ $# -gt 0 ]
then
    SERVER=$1
fi

if [ "$SERVER" = "getactive01" ]
then
    rsync -azC --force --delete --progress --exclude-from=misc/data/rsync_exclude.txt -e "ssh -p50000" ./ root@91.239.66.119:/data/rpg/
elif [ "$SERVER" = "tojad" ]
then
    rsync -azC --force --delete --progress --exclude-from=misc/data/rsync_exclude.txt -e "ssh -p50000" ./ root@91.228.199.109:/data/rpg/
elif [ "$SERVER" == "vm" ]
then
    rsync -azC --force --delete --progress --exclude-from=misc/data/rsync_exclude.txt -e "ssh -p22" ./ root@192.168.56.101:/data/rpg/
fi
