#!/bin/sh

sh misc/bin/project_disable.sh

sleep 30
sudo /etc/init.d/postgresql-9.1 restart
sudo /etc/init.d/mongod restart
sudo /etc/init.d/memcached restart
sudo /etc/init.d/httpd restart
sudo /etc/init.d/crond stop
